<%-- 
    Document   : company
    Created on : Jun 8, 2018, 5:04:09 PM
    Author     : Davey
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="layout/Header2.jsp" />


<!-- Session validation for Room operator  -->
<%
	if(session.getAttribute("Email")==null ){
		response.sendRedirect("loginForm.jsp");
	}
%>
<!-- session validation ends -->


<!-- ***********   START   *********** -->
<div class="left-column col-lg-3 col-md-3 col-sm-3 col-xs-3">
	<ul>
		<li><a href="findRecruit.jsp">Find recruit</a></li>
		<li><a href="myRecruitments.jsp">Show my previouse recruitment</a></li>
		<li><a href="EditProfile.jsp">Edit profile</a></li>
		<li><a href="ChangePassword.jsp">Change Password</a></li>
		<li><a href="deleteAccount.jsp">Delete account</a></li>
		<li><a href="loginForm.jsp">Logout</a>
	</ul>
</div>
<div class="dashboard container col-lg-9 col-md-9 col-sm-9 col-xs-7">
	<div class="dashboardBody col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="formTitle col-lg-12 col-md-12 col-sm-12 col-xs-12">company Dashboard</div>
		<div class="errordiv col-lg-10 col-md-10 col-sm-10 col-xs-10">
				<%
	try {
		String msg1 = "You are Logged in as "+session.getAttribute("Lname");
                String msg="";
		if (request.getParameter("msg") != null) {
			msg = request.getParameter("msg");
			out.print("<div><p>" + msg + "</p></div>");
			System.out.print("kkkk");
		}
                else{
                    out.print("<div><p>" + msg1 + "</p></div>");
                }
	} catch (Exception e) {
	}
%>
</div>
		<div class="dashboarddiv col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="odd col-lg-3 col-md-3 col-sm-3 col-xs-3">
				<div class="dashimg">
                                      <img src="assets/img/w4.jpg"/> 
                                 
					Find Recruits
				</div>
				<div class="dashcontent">
					<a href="findRecruit.jsp">Find</a>
				</div>
			</div>
			<div class="even col-lg-3 col-md-3 col-sm-3 col-xs-3">
				<div class="dashimg">
                                     <img src="assets/img/w1.jpg"/>
					Show Recruited
					</div>
				<div class="dashcontent">					
					<a href="GetAddedRoomes.jsp">Show</a>
				</div>
			</div>
                    <div class="odd  col-lg-3 col-md-3 col-sm-3 col-xs-3">			
				<div class="dashimg">
                                     <img src="assets/img/w6.jpg"/> 
					View Recruitment Report
				</div>
				<div class="dashcontent">
					<a href="DeleteRoomOperator.jsp">View</a>	
				</div>
			</div>
                     <div class="odd  col-lg-3 col-md-3 col-sm-3 col-xs-3">			
				<div class="dashimg">
                                     <img src="assets/img/w5.jpg"/>
					Edit profile
				</div>
				<div class="dashcontent">
					<a href="DeleteRoomOperator.jsp">Edit</a>	
				</div>
			</div>
			<div class="odd  col-lg-3 col-md-3 col-sm-3 col-xs-3">			
				<div class="dashimg">
                                     <img src="assets/img/w7.jpg"/> 
					Change Password
				</div>
				<div class="dashcontent">
					<a href="ChangePassword.jsp">Change</a>	
				</div>
			</div>
			<div class="odd  col-lg-3 col-md-3 col-sm-3 col-xs-3">			
				<div class="dashimg">
                                     <<img src="assets/img/r03.jpg"/> 
					Delete account
				</div>
				<div class="dashcontent">
					<a href="DeleteRoomOperator.jsp">Delete</a>	
				</div>
			</div>
                    
                   
		</div>
	</div>
</div>

<!-- ***********   basha html ends   *********** -->

<jsp:include page="layout/Footer.jsp" />

-------------------------------------------------------------------------------------------------------


