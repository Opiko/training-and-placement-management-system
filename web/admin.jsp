<%-- 
    Document   : admin
    Created on : Jun 8, 2018, 5:04:58 PM
    Author     : Davey
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="layout/Header3.jsp" />
<jsp:include page="layout/AdminSidebar.jsp" />

<!-- Session validation for Room operator  -->
<%
	if(session.getAttribute("Email")==null ){
		response.sendRedirect("loginForm.jsp");
	}
%>
<!-- session validation ends -->


<!-- ***********   START   *********** -->

<div class="dashboard container col-lg-9 col-md-9 col-sm-9 col-xs-7">
	<div class="dashboardBody col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="formTitle col-lg-12 col-md-12 col-sm-12 col-xs-12">Admin Dashboard</div>
		<div class="dashboarddiv col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div>
                        <% try {
		String msg1 = "Welcome Admin,You are Logged in as "+session.getAttribute("Lname");
                String msg="";
		if (request.getParameter("msg") != null) {
			msg = request.getParameter("msg");
			out.print("<div><p>" + msg + "</p></div>");
			System.out.print("kkkk");
		}
                else{
                    out.print("<div><p>" + msg1 + "</p></div>");
                }
	} catch (Exception e) {
	}
                        %>
                    </div>
			<div class="odd col-lg-3 col-md-3 col-sm-3 col-xs-3">
				<div class="dashimg">
                                    <img src="assets/img/w6.jpg"/>
					View Registered companies
                                      
				</div>
				<div class="dashcontent">
					<a href="viewCompanies.jsp">View</a>
				</div>
			</div>
			<div class="even col-lg-3 col-md-3 col-sm-3 col-xs-3">
				<div class="dashimg">
                                    <img src="assets/img/eger009.png"/>
					Create Placement Report
					</div>
				<div class="dashcontent">					
					<a href="addnewRoomoperator.jsp">Create</a>
				</div>
			</div>
			<div class="even col-lg-3 col-md-3 col-sm-3 col-xs-3">
				<div class="dashimg">
                                    <img src="assets/img/eger003.jpg"/>
					Delete Student
					</div>
				<div class="dashcontent">					
					<a href="deleteStudent.jsp">Delete</a>
				</div>
			</div>
			<div class="even col-lg-3 col-md-3 col-sm-3 col-xs-3">
				<div class="dashimg">
                                    <img src="assets/img/eger007.jpg"/>
					View Registered Student
					</div>
				<div class="dashcontent">					
					<a href="ViewRegisteredUsers.jsp">View</a>
				</div>
			</div>
			<div class="even col-lg-3 col-md-3 col-sm-3 col-xs-3">
				<div class="dashimg">
                                    <img src="assets/img/eger008.jpg"/>
					View Placements
					</div>
				<div class="dashcontent">					
					<a href="ViewAllBookings.jsp">View</a>
				</div>
			</div>
			<div class="even col-lg-3 col-md-3 col-sm-3 col-xs-3">
				<div class="dashimg">
                                    <img src="assets/img/w7.jpg"/>
					Search Student
					</div>
				<div class="dashcontent">					
					<a href="editStudent.jsp">Search</a>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- ***********   html ends   *********** -->

<jsp:include page="layout/Footer.jsp" />

