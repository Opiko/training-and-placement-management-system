<%-- 
    Document   : registerStudent
    Created on : Jun 22, 2018, 1:11:10 PM
    Author     : Davey
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="libraryy.connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="layout/StudentHeader.jsp" />
<script type="text/javascript" src="assets/js/blank.js"></script>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register Student</title>
    </head>
    <body>
        <div class="mymainWrapper col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<div class="centerformWrapper col-lg-4 col-md-6 col-sm-12 col-xs-12">
		<div class="centertitle row col-lg-12 col-md-12 col-sm-12 col-xs-12">
			Student Registration form
                </div>
		<div class="errordiv col-lg-10 col-md-10 col-sm-10 col-xs-10">
		<%
			String msg= request.getParameter("msg");
			if(msg!=null){
				out.print(msg);
			}else{%>
                        <h4>Please Put in your Correct Details.</h4>
                               <% }
                 try{
                 connection conn=new connection();
                // connection conn1=new connection();

                    Connection con;
                    //Connection con1;
                    
                    con=conn.connect();
                   //con1 = conn1.connect();

             
             String query="select * from Courses";
             //String query1="SELECT Cid,DISTINCT Faculty FROM courses";

             Statement st=con.createStatement();
            //Statement st1=con1.createStatement();
             
             ResultSet rst =null;
            // ResultSet rst1 =null;

                rst=st.executeQuery(query);
               // rst=st1.executeQuery(query1);
               
                      
		%>
		</div>
                <div class="centerform col-lg-12 col-md-12 col-sm-12c col-xs-12">
		<form action="studentRegisterController"
                      onsubmit="return validateLogin15();" method="post">
				
			
				<div class="form-group">
					<label for="Fname">First Name</label> 
					<input type="text" name="Fname" class="form-control required"/>
				</div>
                   
                    <div class="form-group">
					<label for="Lname">Last Name</label> 
					<input type="text" name="Lname" class="form-control required"/>
				</div>
                     
                   
                    
                     <div class="form-group">
					<label for="email">Phone Number</label> 
					<input type="text" name="Phone" class="form-control required "/>
				</div>
                    <div class="">
					<label for="Phone">Your Email Address</label> 
					<input type="text" placeholder="xyz@gmail.com" name="Email"  class="form-control required"/>
				</div>
                    		
                    <div class="form-group">
					<label for="DoB">Age</label> 
					<input class="form-control" placeholder="Above 18yrs"  min="18" max= "35" type="number" name="DoB" required />
				</div>
                    
                     <div class="form-group">
					<label for="gender">Select your gender.</label>
					<select name="gender" id="product" class="form-control">
						<option value="Male">Male</option>
						<option value="Female">Female</option>	
					</select>
				</div>
                   
                    <div class="form-group">
					<label for="level">Which level of Study?</label>
					<select name="level" id="product" class="form-control">
						<option value="Degree">Degree</option>
						<option value="Diploma">Diploma</option>
                                                <option value="Certificate">Certificate</option>
					</select>
				</div>
                    <%-- <div class="form-group">
					<label for="course">select your Area of study</label>
                                        
					<select name="Area" id="product" class="form-control">
                                            <%
                                        //while(rst1.next())
                                        //{
                                        %>
                                            <option value="<%=//rst1.getInt("Cid")%>"><%=//rst1.getString("faculty")%></option>
						<%
                                               // }
                                              // st1.close(); 
                                               // rst1.close(); 
                                                %>
					</select>
</div>--%>
                    
                    <div class="form-group">
					<label for="course">select your course of study</label>
                                        
					<select name="course" id="product" class="form-control">
                                            <%
                                        while(rst.next())
                                        {
                                        %>
                                        <option value="<%=rst.getString("Cname")%>"><%=rst.getString("Cname")%></option>
                                       <%-- <option value="<%=rst.getInt("Cid")%>"><%=rst.getString("Cname")%></option>--%>
						<%
                                                }}catch(Exception e){
                                                       e.printStackTrace();
                                            }
                                                %>
					</select>
				</div>
                    <div class="form-group">
					<label for="school">select your school</label>
					<select name="school" id="product" class="form-control">
						<option value="Egerton">Egerton</option>
						<option value="KU">KU</option>
                                                <option value="UoN">UoN</option>
					</select>
				</div>
                    
                    
                   <div class="form-group">
					<label for="VPassword">Enter Password</label> 
					<input type="password" name="Pswd"  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter and at least 8 or more characters" placeholder="Enter a minimum of 8 characters" class="form-control"/>
				</div>
                <div class="form-group">
					<label for="VPassword">confirm Password</label> 
					<input type="password" name="VPswd"  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter and at least 8 or more characters" placeholder="Must be the same as the above" class="form-control"/>
				</div>
                     
                 <div class="form-group">
					<label for="Sq">Select Security Question.</label>
					<select name="sq" id="product" class="form-control">
						<option value="Which town are you from?">Which town are you from?</option>
						<option value="Name your Primary school">Name your Primary school</option>
                                                <option value="Which place do you like to visit?">Which place do you like to visit?</option>
					</select>
				</div>
                <div class="form-group">
					<label for="SA">Security answer</label> 
					<input type="text" name="sa" class="form-control"/>
				</div>
                  
                    
                    <div class="form-group">
                            
                                <button type="reset" class="btn btn-danger">Reset</button>
					<button type="submit" class="btn btn-danger">Submit</button>
                                        
                                    
					
				</div>
			</form>
                    	</div>
	</div>
</div>
    </body>
</html>
