
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<!-- ***********   START   *********** -->
<jsp:include page="layout/StudentHeader.jsp" />
<script type="text/javascript" src="assets/js/blank.js"></script>

<div class="mymainWrapper col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<div class="centerformWrapper col-lg-4 col-md-6 col-sm-12 col-xs-12">
		<div class="centertitle row col-lg-12 col-md-12 col-sm-12 col-xs-12">
			Add student's GPA
		</div>
		<div class="errordiv col-lg-10 col-md-10 col-sm-10 col-xs-10">
		<%
			String msg= request.getParameter("msg");
			if(msg!=null){
				out.print(msg);
			}
		%>
		</div>
		<div class="centerform col-lg-12 col-md-12 col-sm-12c col-xs-12">
		<form name="edit" action="Newservlet?action=editGPA" method="post">
				
			                    <div class="form-group">
				<div class="form-group">
					<label for="fname">Enter the student's GPA</label> 
					<input type="text" name="gpa" />
				</div>

			                    <div class="form-group">
		
                                                <input type="hidden" name="Email" value='<%=request.getParameter("Email")%>' />
				</div>
                  
                   
				<div class="form-group">
                                    
                                    <button type="reset" class="btn btn-danger">Reset</button>
					<button type="submit" class="btn btn-danger">Submit</button>
					</div>
				</div>
			</form>
                                        
		
         
	</div>
          
</div>

</body>
<!-- ********************************* -->
<jsp:include page="layout/Footer.jsp" />
