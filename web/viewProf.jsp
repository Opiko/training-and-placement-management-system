<%-- 
    Document   : viewStPfof
    Created on : Jul 25, 2018, 11:04:41 AM
    Author     : Davey
--%>

<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="libraryy.connection"%>
<!DOCTYPE html>
<html>

    <head>
        <%
            try{
            if (session.getAttribute("Email") == null) {
                response.sendRedirect("loginForm.jsp");
            } else {
                
                
                 connection conn=new connection();
    Connection con;
             con = conn.connect();
            
             
             String query1="select * from mystudents where Email='"+session.getAttribute("Email")+"'";
             Statement st=con.createStatement();
             
             ResultSet rst =st.executeQuery(query1);
              
     
                  
                while(rst.next()){
            
     

        %>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Personal information</title>

        <link href="assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/css/datepicker3.css" rel="stylesheet">
        <link href="assets/css/styles.css" rel="stylesheet">

        <!--Icons-->
        <script src="js/lumino.glyphs.js"></script>


    </head>

    <body>


        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><span>USER</span>PROFILE</a>
                    <ul class="user-menu">
                        <li class="dropdown pull-right">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> <%=rst.getString("Fname")%> <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">

                                <li><a href="logout.jsp"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>

            </div><!-- /.container-fluid -->
        </nav>

        <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
            <form role="search">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
            </form>
            <ul class="nav menu">
                <li class="active"><a href="editProf.jsp"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg>Edit Personal information</a></li>
                <li><a href="placementRepo.jsp"><svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg> View Placement Report</a></li>
                <li><a href="viewCompanies.jsp"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-line-graph"></use></svg>View Companies</a></li>
                <li><a href="loginForm.jsp"><svg class="glyph stroked table"><use xlink:href="#stroked-table"></use></svg>Logout</a></li>


            </ul>

        </div><!--/.sidebar-->

        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			

            <h1>PERSONAL INFORMATION</h1>
            <div style="border-style:solid; border-width:thin;margin:auto;  padding:5px; background-color:white">
                <form>

                    <table  style="align:auto; width:70%;">
                        <tr>
                            <td >Your Name		: </td><td style="padding:10px"><%=rst.getString("Fname")%></td>
                            <td style="padding:10px"><%=rst.getString("Lname")%></td>

                        </tr>
                        <tr>
                            <td>Your Email Address		: </td><td style="padding:20px"> <%=rst.getString("Email")%></td>
                        </tr>
                        <tr>
                            <td>Contact Number		: </td><td style="padding:20px"> <%=rst.getString("phone")%></td>
                        </tr>

                        <tr>
                            <td >Age		: </td><td style="padding:20px"> <%=rst.getString("DoB")%></td>
                        </tr>

                        <tr>
                            <td>Gender		: </td> <td style="padding:20px"><%=rst.getString("gender")%></td>
                        </tr>

                        <tr>
                            <td >Level of study		:</td> <td style="padding:20px"> <%=rst.getString("level")%></td>
                        </tr>

                        <tr>
                            <td >Course of study 		: </td><td style="padding:20px" > <%=rst.getString("course")%></td>
                        </tr>
                        <tr>
                            <td>School		: </td><td style="padding:20px"> <%=rst.getString("school")%></td>
                        </tr>
                        <tr>
                            <td>Your GPA		: </td><td style="padding:20px"> <%=rst.getString("GPA")%></td>
                        </tr>

<%
    }
}
}catch(Exception e){
e.printStackTrace();
}
%>


                        <tr>

                            <td style="padding:20px"><a href="ResetPswd.jsp">Change password</a></td>
                        </tr>
                    </table>



                </form>
            </div>

        </div><!--/.main-->

        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/chart.min.js"></script>
        <script src="js/chart-data.js"></script>
        <script src="js/easypiechart.js"></script>
        <script src="js/easypiechart-data.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script>
            !function ($) {
                $(document).on("click", "ul.nav li.parent > a > span.icon", function () {
                    $(this).find('em:first').toggleClass("glyphicon-minus");
                });
                $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
            }(window.jQuery);

            $(window).on('resize', function () {
                if ($(window).width() > 768)
                    $('#sidebar-collapse').collapse('show')
            })
            $(window).on('resize', function () {
                if ($(window).width() <= 767)
                    $('#sidebar-collapse').collapse('hide')
            })
        </script>	
    </body>

</html>
