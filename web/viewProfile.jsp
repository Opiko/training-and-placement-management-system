
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="libraryy.connection"%>
<!DOCTYPE html>
<html>

<head>
<%
if(session.getAttribute("Email")==null){
	response.sendRedirect("login.jsp");
}

%>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Personal information</title>

<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/datepicker3.css" rel="stylesheet">
<link href="assets/css/styles.css" rel="stylesheet">

<!--Icons-->
<script src="js/lumino.glyphs.js"></script>

       <%
           try{
    connection conn=new connection();
    Connection con;
             con = conn.connect();
            
             
             String query1="select * from mystudents where Email='"+session.getAttribute("Email")+"'";
             Statement st=con.createStatement();
             
             ResultSet rst =st.executeQuery(query1);
              
     
                  
                while(rst.next()){
             
   
    
%>
</head>

<body>


	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><span>USER</span>PROFILE</a>
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> <%=session.getAttribute("name")%> <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							
							<li><a href="logout.jsp"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
							
		</div><!-- /.container-fluid -->
	</nav>
		
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li class="active"><a href="PerInformation.jsp"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg>Personal Information</a></li>
			<li><a href="AssignedTask.jsp"><svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg> Edit Personal Information</a></li>
			<li><a href="comments.jsp"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-line-graph"></use></svg>Contact Us</a></li>
			<li><a href="Permision.jsp"><svg class="glyph stroked table"><use xlink:href="#stroked-table"></use></svg> Request for Placement</a></li>
			<li><a href="UserViewNote.jsp"><svg class="glyph stroked table"><use xlink:href="#stroked-table"></use></svg> View Placement Report</a></li>
			
			
		</ul>
		
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		
		<h1>PERSONAL INFORMATION</h1>
		<div style="border-style:solid; border-width:thin;margin:auto;  padding:5px; background-color:white">
		<form>
		
		<table  style="align:auto; width:70%;">
		<tr>
	<td >Your Name		: </td><td style="padding:20px"><%=rst.getString("Fname")%></td>
	
	</tr>
	
		<tr>
		<td >Year of Birth		: </td><td style="padding:20px"> <%=rst.getString("DoB")%></td>
		</tr>
		
		<tr>
		<td>Email Address		: </td> <td style="padding:20px"><%=rst.getString("Email")%></td>
		</tr>
		
		<tr>
		<td >Your Phone Number		:</td> <td style="padding:20px"> <%=rst.getString("phone")%></td>
		</tr>
		
		<tr>
		<td >Depatment 		: </td><td style="padding:20px" > <%=rst.getString("RegNo")%></td>
		</tr>
		
		<tr>
                    <td>Your Email Address		: </td><td style="padding:20px"> <%=rst.getString("RegNo")%><%session.getAttribute("Email");%></td>
		</tr>
		
		<tr>
		
		<td style="padding:20px"><a href="ResetPswd.jsp">Change password</a></td>
		</tr>
		</table>
		
		
		
		</form>
		</div>
                <%
                   } 
                }catch(Exception e){
e.printStackTrace();
}
                
                %>
		
	</div><!--/.main-->

	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script>
		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
				$(this).find('em:first').toggleClass("glyphicon-minus");	  
			}); 
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>	
</body>

</html>
