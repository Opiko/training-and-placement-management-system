<%-- 
    Document   : logout
    Created on : Jun 12, 2018, 11:21:45 AM
    Author     : Davey
--%>

 <%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>logged out</title>
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/datepicker3.css" rel="stylesheet">
<link href="assets/css/styles.css" rel="stylesheet">

<!--Icons-->
<script src="assets/js/lumino.glyphs.js"></script>
</head>
<body>

<%session.invalidate();%>
<div style="border-style:solid; border-width:thin;margin:auto;  padding:15px; background-color:red "><h3>You've successfully logged out 
 </h3></div>
 <div style="border-style:solid;border-width: thick;margin:  auto;  padding:20px; background-color:greenyellow">
 <a href="loginForm.jsp" class="pull-right">Click here to go to login page</a>
 </div>

</body>
</html>
