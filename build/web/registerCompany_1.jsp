<%-- 
    Document   : registeCompany
    Created on : Jun 18, 2018, 3:31:22 PM
    Author     : Davey
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="layout/CompanyHeader.jsp" />
<script type="text/javascript" src="assets/js/blank.js"></script>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register Company</title>
    </head>
    <body>
        <div class="mymainWrapper col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<div class="centerformWrapper col-lg-4 col-md-6 col-sm-12 col-xs-12">
		<div class="centertitle row col-lg-12 col-md-12 col-sm-12 col-xs-12">
			Company Registration Form
                </div>
		<div class="errordiv col-lg-10 col-md-10 col-sm-10 col-xs-10">
		<%
			String msg= request.getParameter("msg");
			if(msg!=null){
				out.print(msg);
			}
		%>
		</div>
		<div class="centerform col-lg-12 col-md-12 col-sm-12c col-xs-12">
		<form name="red" action="companyRegisterCotroller" method="post">
				
			
				<div class="form-group">
					<label for="Cname">company name</label> 
					<input type="text" name="Cname" class="form-control"/>
				</div>
                   
                    <div class="form-group">
					<label for="email">Company Email</label> 
					<input type="text" name="CEmail" class="form-control"/>
				</div>
                    <div class="form-group">
					<label for="uname"> company Reg. Number</label> 
					<input type="text" name="RegNo" class="form-control"/>
				</div>
                    <div class="form-group">
					<label for="phone">Company Phone Contact</label> 
					<input type="text" name="Phone" class="form-control"/>
				</div>
                    
                    <div class="form-group">
					<label for="phoneNumber">Company Location</label> 
					<input type="text" name="Location" class="form-control"/>
				</div>
                   <div class="form-group">
					<label for="Type">What type is your company</label>
					<select name="Type" id="Type" class="form-control">
						<option value="private">Private Limited</option>
						<option value="Public">Public limited</option>
                                                
						
					</select>
				</div>
                    <div class="form-group">
					<label for="Type">Are you registering a company?</label>
					<select name="Role" id="Type" class="form-control">
						<option value="Company">Yes</option>
					</select>
				</div>
                    <div class="form-group">
					<label for="Product">Which Products are you focused on?</label>
					<select name="Product" id="product" class="form-control">
						<option value="Softwares">Softwares</option>
						<option value="Hardwares">Hardwares</option>
                                                <option value="marketing">marketing</option>
                                                <option value="Others">Others</option>
						
					</select>
				</div>
                     <div class="form-group">
					<label for="HRManager">Name of the HR Manager</label> 
					<input type="text" name="HRManager" class="form-control"/>
				</div>
				<div class="form-group">
					<label for="password">Password</label> 
					<input type="password" name="password" class="form-control"/>
				</div>
				<div class="form-group">
					<label for="confirmPassword">Confirm Password</label> 
					<input type="password" name="vpassword" class="form-control"/>
				</div>
				
				
				<div class="form-group">
					<label for="sq">Security Question</label>
					<select name="sq" id="sq" class="form-control">
						<option value="When was the company formed (month)">When was the company formed (month)?</option>
						<option value="Name a stakeholder of this company you trust most?">Name one stakeholder of this company you trust most?</option>
						
					</select>
				</div>
				<div class="form-group">
					<label for="sa">Security Answer</label> 
					<input type="text" name="sa" class="form-control"/>
				</div>
				<div class="form-group">
                                    <button type="reset" class="btn btn-danger">Reset</button>
					<button type="submit" class="btn btn-danger">Submit</button>
					
				</div>
			</form>
		</div>
	</div>
</div>
    </body>
</html>
