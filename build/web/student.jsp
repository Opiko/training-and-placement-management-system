<%-- 
    Document   : student
    Created on : Jun 8, 2018, 5:02:46 PM
    Author     : Davey
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="layout/StudentHeader.jsp" />
<jsp:include page="layout/StudentSidebar.jsp" />

<!-- Session validation for Room operator  -->
<%
	if(session.getAttribute("Email")==null ){
		response.sendRedirect("loginForm.jsp");
	}
%>
<!-- session validation ends -->


<!-- ***********   START   *********** -->

<div class="dashboard container col-lg-9 col-md-9 col-sm-12 col-xs-12">
	<div class="dashboardBody col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="formTitle col-lg-12 col-md-12 col-sm-12 col-xs-12">Student Dashboard</div>
                <div class="errordiv col-lg-10 col-md-10 col-sm-10 col-xs-10">
		<%
	try {
		String msg1 = "You are Logged in as "+session.getAttribute("Lname");
                String msg="";
		if (request.getParameter("msg") != null) {
			msg = request.getParameter("msg");
			out.print("<div><p>" + msg + "</p></div>");
			System.out.print("kkkk");
		}
                else{
                    out.print("<div><p>" + msg1 + "</p></div>");
                }
	} catch (Exception e) {
	}
%>
</div>
		<div class="dashboarddiv col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="odd col-lg-3 col-md-4 col-sm-12 col-xs-12">
				<div class="dashimg">
                                     <img src="assets/img/eger008.jpg"/> 
					<span class="glyphicon glyphicon-search"></span>Search companies
				</div>
				<div class="dashcontent">
					<a href="SearchRoomForm.jsp">Search</a>
				</div>
			</div>
			 <div class="even  col-lg-3 col-md-3 col-sm-12 col-xs-12">						
				<div class="dashimg">
					 <img src="assets/img/pl2.jpg"/> 
					<span class="glyphicon glyphicon-eye-open "></span>View Profile
				</div>
				<div class="dashcontent">
					<a href="viewProfile.jsp">View</a>
				</div>
			</div>
			
                    <div class="even  col-lg-3 col-md-3 col-sm-12 col-xs-12">						
				<div class="dashimg">
					 <img src="assets/img/w3.jpg"/> 
					<span class="glyphicon glyphicon-eye-open "></span>View Recruitment report
				</div>
				<div class="dashcontent">
					<a href="CancelReceipt.jsp">View</a>
				</div>
			</div>
			<div class="even  col-lg-3 col-md-3 col-sm-12 col-xs-12">						
				<div class="dashimg">
					 <img src="assets/img/cv.jpg"/> 
					<span class="glyphicon "></span>Upload CV
				</div>
				<div class="dashcontent">
					<a href="CancelReceipt.jsp">Upload</a>
				</div>
			</div>
                    <div class="odd  col-lg-3 col-md-3 col-sm-12 col-xs-12">
				<div class="dashimg">
                                     <img src="assets/img/pl1.jpg"/> 
				<span class="glyphicon "></span>Edit profile
				</div>
				<div class="dashcontent">
					<a href="GetBookingInformation.jsp">Edit</a>
				</div>			
			</div>
			<div class="odd  col-lg-3 col-md-3 col-sm-12 col-xs-12">
				<div class="dashimg">
                                     <img src="assets/img/r06.png"/> 
				<span class="glyphicon "></span>Sign out
				</div>
				<div class="dashcontent">
					<a href="logout.jsp">Sign out</a>
				</div>			
			</div>
		</div>
	</div>
</div>

<!-- ***********   basha html ends   *********** -->

<jsp:include page="layout/Footer.jsp" />


