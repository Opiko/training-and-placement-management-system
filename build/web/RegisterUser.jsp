
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<!-- ***********   START   *********** -->
<jsp:include page="layout/StudentHeader.jsp" />
<script type="text/javascript" src="assets/js/blank.js"></script>

<div class="mymainWrapper col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<div class="centerformWrapper col-lg-4 col-md-6 col-sm-12 col-xs-12">
		<div class="centertitle row col-lg-12 col-md-12 col-sm-12 col-xs-12">
			STUDENT REGISTRATION FORM
		</div>
		<div class="errordiv col-lg-10 col-md-10 col-sm-10 col-xs-10">
		<%
			String msg= request.getParameter("msg");
			if(msg!=null){
				out.print(msg);
			}
		%>
		</div>
		<div class="centerform col-lg-12 col-md-12 col-sm-12c col-xs-12">
		<form name="register" action="myRegisterController" method="post">
				
			
				<div class="form-group">
					<label for="fname">First Name</label> 
					<input type="text" name="Fname" class="form-control"/>
				</div>
                    <div class="form-group">
					<label for="lName">Last Name</label> 
					<input type="text" name="Lname" class="form-control"/>
				</div>
                  
                    <div class="form-group">
					<label for="email">Email</label> 
					<input type="text" name="email" class="form-control"/>
				</div>
                    <div class="form-group">
					<label for="reg">Reg. Number</label> 
					<input type="text" name="RegNo" class="form-control"/>
				</div>
                    <div class="form-group">
					<label for="phone">Phone</label> 
					<input type="text" name="Phone" class="form-control"/>
				</div>
                    <div class="form-group">
					<label for="phone">Year of Birth</label> 
					<input type="text" name="DoB" class="form-control"/>
				</div>
                    
                    <div class="form-group">
					<label for="phoneNumber">Resident</label> 
					<input type="text" name="Resident" class="form-control"/>
				</div>
                   <div class="form-group">
					<label for="Couirse">Course</label>
					<select name="Course" id="Role" class="form-control">
						<option value="Bsc">Bsc</option>
						<option value="BA">BA</option>
                                                <option value="BED">BED</option>
						
					</select>
				</div>
                   
				<div class="form-group">
					<label for="password">Password</label> 
					<input type="password" name="password" class="form-control"/>
				</div>
				<div class="form-group">
					<label for="confirmPassword">Confirm Password</label> 
					<input type="password" name="vpassword" class="form-control"/>
				</div>
				
				
				<div class="form-group">
					<label for="sq">Security Question</label>
					<select name="sq" id="sq" class="form-control">
						<option value="What was the name of your elementary / primary school?">What was the name of your elementary / primary school?</option>
						<option value="What was your favorite place to visit as a child?">What was your favorite place to visit as a child?</option>
						<option value="What month were you born?">What month were you born?</option>
					</select>
				</div>
				<div class="form-group">
					<label for="sa">Security Answer</label> 
					<input type="text" name="sa" class="form-control"/>
				</div>
				<div class="form-group">
                                    
                                    <button type="reset" class="btn btn-danger">Reset</button>
					<button type="submit" class="btn btn-danger">Submit</button>
					
				</div>
			</form>
                                                 <div class="form-group" style="width:300px;margin-left:290px;font-family:timesnewroman; font-size:100%;color:#003300;margin:0 auto">
                                                     <p><b> REGISTER COMPANY <a href="registerCompany.jsp"> HERE</a></b></p>
    </div>
		</div>
         
	</div>
          
</div>

</body>
<!-- ********************************* -->
<jsp:include page="layout/Footer.jsp" />
<%-- <div class="form-group">
					<label for="DoB">Year of Birth</label> 
					<input type="text" name="DoB" class="form-control"/>
				</div>
                    
                    
                   
                    <div class="form-group">
					<label for="password">Enter Password</label> 
					<input type="password" name="Password" class="form-control"/>
				</div>
                    <div class="form-group">
					<label for="vpassword">Verify Password</label> 
					<input type="password" name="vpassword" class="form-control"/>
				</div>
                    <div class="form-group">
					<label for="sq">Security Question</label>
					<select name="sq" id="sq" class="form-control">
						<option value="What was the name of your elementary / primary school?">What was the name of your elementary / primary school?</option>
						<option value="What was your favorite place to visit as a child?">What was your favorite place to visit as a child?</option>
						<option value="What month were you born?">What month were you born?</option>
					</select>
				</div>
                    <div class="form-group">
					<label for="sa">Security Answer</label> 
					<input type="text" name="sa" class="form-control"/>
				</div>
--%>