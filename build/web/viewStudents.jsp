<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="libraryy.connection"%>
<%@ page import="java.sql.ResultSet" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Companies</title>
        <jsp:include page="layout/Header3.jsp" />
    </head>
    <body>
        <%
    connection conn=new connection();
    Connection con;
             con = conn.connect();
             String query="select * from mystudents order by Fname";
             Statement st=con.createStatement();
             ResultSet rs=st.executeQuery(query);
   
    
%>
<div class="mymainWrapper col-lg-9 col-md-9 col-sm-12 col-xs-12">
    <div class="tableWrapper col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="centertitle row col-lg-12 col-md-12 col-sm-12 col-xs-12">
         Registered Students
        </div>
        <div class="tableContent col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <table class ="table table-bordered">
                <tr>
                    <td>First Name</td>
                    <td>Last Name</td>
                    <td>Contact</td>
                    <td>Email Address</td>
                    <td>Gender</td>
                    <td>Level</td>
                    <td>Course</td>
                    <td>GPA</td>
                    

                </tr>

                <%   

                while(rs.next())
                {
                %>
                <tr>
                    <td><%= rs.getString("Fname")%></td>
                    <td><%= rs.getString("Lname")%></td>
                    <td><%=rs.getString("phone")%></td>
                    <td><%=rs.getString("Email")%></td>
                    <td><%=rs.getString("gender")%></td>
                    <td><%=rs.getString("level")%></td>
                    <td><%=rs.getString("course")%></td>
                    <td><%=rs.getString("GPA")%></td>
                   
                    
                    			
                </tr>

                <%}%>
               
                
            </table>
        </div>
    </div>
</div>
<!-- ********************************* -->
<jsp:include page="layout/Footer.jsp" />
    </body>
</html>
