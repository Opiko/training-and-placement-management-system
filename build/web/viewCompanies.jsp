<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="libraryy.connection"%>
<%@ page import="java.sql.ResultSet" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Companies</title>
        <jsp:include page="layout/Header3.jsp" />
             <style type="text/css">

.customy {
	font-family: Courier;
	color: blue;
        text-decoration: darkmagenta;
	font-size:20px;
      
}


</style>
  </head>
    <body>
        <%
    connection conn=new connection();
    Connection con;
             con = conn.connect();
             String query="select * from companies order by CEmail ";
             Statement st=con.createStatement();
             ResultSet rs=st.executeQuery(query);
   
    
%>
<div class="mymainWrapper col-lg-9 col-md-9 col-sm-12 col-xs-12">
    <div class="tableWrapper col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="centertitle row col-lg-12 col-md-12 col-sm-12 col-xs-12">
          List of Registered Companies
        </div>
        <div class="tableContent col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <table class ="table table-bordered">
                <tr>
                    <td>Company Name</td>
                    <td>Email Address</td>
                    <td>Contact</td>
                    <td>Product Type</td>
                    <td>HR Manager</td>
                     <td>Location</td>
                    

                </tr>

                <%   

                while(rs.next())
                {
                %>
                <tr>
                    <td><%= rs.getString("Cname")%></td>
                    <td><%=rs.getString("CEmail")%></td>
                    <td><%=rs.getString("Phone")%></td>
                    <td><%=rs.getString("Product")%></td>
                    <td><%=rs.getString("HRManager")%></td>
                    <td><%=rs.getString("Location")%></td>
                   
                    
                    			
                </tr>

                <%}%>
               
                
            </table>
        </div>
    </div>
</div>
<!-- ********************************* -->
<jsp:include page="layout/Footer.jsp" />
    </body>
</html>
