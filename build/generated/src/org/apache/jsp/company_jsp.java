package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class company_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "layout/Header2.jsp", out, false);
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!-- Session validation for Room operator  -->\n");
      out.write("\n");
      out.write("<!-- session validation ends -->\n");
      out.write("\n");
      out.write("\n");
      out.write("<!-- ***********   START   *********** -->\n");
      out.write("<div class=\"left-column col-lg-3 col-md-3 col-sm-3 col-xs-3\">\n");
      out.write("\t<ul>\n");
      out.write("\t\t<li><a href=\"findRecruits.jsp\">Find recruit</a></li>\n");
      out.write("\t\t<li><a href=\"shoeRecruitment.jsp\">Show recruitment details</a></li>\n");
      out.write("\t\t<li><a href=\"EditProfile.jsp\">Edit profile</a></li>\n");
      out.write("\t\t<li><a href=\"ChangePassword.jsp\">Change Password</a></li>\n");
      out.write("\t\t<li><a href=\"deleteAccount.jsp\">Delete account</a></li>\n");
      out.write("\t\t<li><a href=\"logout.jsp\">Logout</a>\n");
      out.write("\t</ul>\n");
      out.write("</div>\n");
      out.write("<div class=\"dashboard container col-lg-9 col-md-9 col-sm-9 col-xs-7\">\n");
      out.write("\t<div class=\"dashboardBody col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n");
      out.write("\t\t<div class=\"formTitle col-lg-12 col-md-12 col-sm-12 col-xs-12\">company Dashboard</div>\n");
      out.write("\t\t<div class=\"errordiv col-lg-10 col-md-10 col-sm-10 col-xs-10\">\n");
      out.write("\t\t");

	try {
		String msg = "";
		if (request.getParameter("msg") != null) {
			msg = request.getParameter("msg");
			out.print("<div><p>" + msg + "</p></div>");
			System.out.print("kkkk");
		}
	} catch (Exception e) {
	}

      out.write("\n");
      out.write("</div>\n");
      out.write("\t\t<div class=\"dashboarddiv col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n");
      out.write("\t\t\t<div class=\"odd col-lg-3 col-md-3 col-sm-3 col-xs-3\">\n");
      out.write("\t\t\t\t<div class=\"dashimg\">\n");
      out.write("                                      <img src=\"assets/img/w4.jpg\"/> \n");
      out.write("                                 \n");
      out.write("\t\t\t\t\tFind Recruits\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t\t<div class=\"dashcontent\">\n");
      out.write("\t\t\t\t\t<a href=\"findRecruits.jsp\">Find</a>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t\t<div class=\"even col-lg-3 col-md-3 col-sm-3 col-xs-3\">\n");
      out.write("\t\t\t\t<div class=\"dashimg\">\n");
      out.write("                                     <img src=\"assets/img/w1.jpg\"/>\n");
      out.write("\t\t\t\t\tShow Recruited\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t<div class=\"dashcontent\">\t\t\t\t\t\n");
      out.write("\t\t\t\t\t<a href=\"GetAddedRoomes.jsp\">Show</a>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t</div>\n");
      out.write("                    <div class=\"odd  col-lg-3 col-md-3 col-sm-3 col-xs-3\">\t\t\t\n");
      out.write("\t\t\t\t<div class=\"dashimg\">\n");
      out.write("                                     <img src=\"assets/img/w6.jpg\"/> \n");
      out.write("\t\t\t\t\tView Recruitment Report\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t\t<div class=\"dashcontent\">\n");
      out.write("\t\t\t\t\t<a href=\"DeleteRoomOperator.jsp\">View</a>\t\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t</div>\n");
      out.write("                     <div class=\"odd  col-lg-3 col-md-3 col-sm-3 col-xs-3\">\t\t\t\n");
      out.write("\t\t\t\t<div class=\"dashimg\">\n");
      out.write("                                     <img src=\"assets/img/w5.jpg\"/>\n");
      out.write("\t\t\t\t\tEdit profile\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t\t<div class=\"dashcontent\">\n");
      out.write("\t\t\t\t\t<a href=\"DeleteRoomOperator.jsp\">Edit</a>\t\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t\t<div class=\"odd  col-lg-3 col-md-3 col-sm-3 col-xs-3\">\t\t\t\n");
      out.write("\t\t\t\t<div class=\"dashimg\">\n");
      out.write("                                     <img src=\"assets/img/w7.jpg\"/> \n");
      out.write("\t\t\t\t\tChange Password\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t\t<div class=\"dashcontent\">\n");
      out.write("\t\t\t\t\t<a href=\"ChangePassword.jsp\">Change</a>\t\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t\t<div class=\"odd  col-lg-3 col-md-3 col-sm-3 col-xs-3\">\t\t\t\n");
      out.write("\t\t\t\t<div class=\"dashimg\">\n");
      out.write("                                     <<img src=\"assets/img/r03.jpg\"/> \n");
      out.write("\t\t\t\t\tDelete account\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t\t<div class=\"dashcontent\">\n");
      out.write("\t\t\t\t\t<a href=\"DeleteRoomOperator.jsp\">Delete</a>\t\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t</div>\n");
      out.write("                    \n");
      out.write("                   \n");
      out.write("\t\t</div>\n");
      out.write("\t</div>\n");
      out.write("</div>\n");
      out.write("\n");
      out.write("<!-- ***********   basha html ends   *********** -->\n");
      out.write("\n");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "layout/Footer.jsp", out, false);
      out.write("\n");
      out.write("\n");
      out.write("-------------------------------------------------------------------------------------------------------\n");
      out.write("\n");
      out.write("\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
