package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class loginForm_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("<!DOCTYPE HTML>\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<title>User Login</title>\r\n");
      out.write("<!-- Custom Theme files -->\r\n");
      out.write("<link href=\"assets/css/styleL.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\"/>\r\n");
      out.write("<!-- Custom Theme files -->\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n");
      out.write("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /> \r\n");
      out.write("<meta name=\"keywords\" content=\"Ensaluto Form Responsive,Login form web template, Sign up Web Templates, Flat Web Templates, Login signup Responsive web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design\" />\r\n");
      out.write("<!--Google Fonts-->\r\n");
      out.write("<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>\r\n");
      out.write("<!--Google Fonts-->\r\n");
      out.write("<script src=\"js/jquery.min.js\"></script>\r\n");
      out.write("</head>\r\n");
      out.write("<body class=\"cam-img\">\r\n");
      out.write("<!--log in form start here-->\r\n");
      out.write("<div class=\"app\">\r\n");
      out.write("\t<div class=\"top-bar\">\r\n");
      out.write("\t\t\t<div class=\"navg\">\r\n");
      out.write("\t\t\t\t\r\n");
      out.write("\t\t\t\r\n");
      out.write("\t\t\t\t<script>\r\n");
      out.write("                                  $( \"span.menu\").click(function() {\r\n");
      out.write("                                                                    $(  \"ul.res\" ).slideToggle(\"slow\", function() {\r\n");
      out.write("                                                                     // Animation complete.\r\n");
      out.write("                                                                     });\r\n");
      out.write("                                                                     });\r\n");
      out.write("\t\t       </script>\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write("\t\t<h2 align=\"center\">User login</h2>\r\n");
      out.write("                \r\n");
      out.write("\t</div>\r\n");
      out.write("    <div class=\"forgotpwd\">\r\n");
      out.write("\t\t\t<!-- code to display error message  -->\r\n");
      out.write("\t\t\t");
if(request.getParameter("msg")!=null){
      out.write("\r\n");
      out.write("\t\t\t\t<h4>");
      out.print(request.getParameter("msg") );
      out.write("</h4>\r\n");
      out.write("                                 \r\n");
      out.write("\t\t\t");
}else{
      out.write("\r\n");
      out.write("                        <h3>Please Put in your Login Details.</h3>\r\n");
      out.write("                               ");
 }
                        
      out.write("\r\n");
      out.write("\t\t\t<!-- error message code ends here -->\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t<div class=\"cam-img\">\r\n");
      out.write("\t\t \r\n");
      out.write("\t</div>\r\n");
      out.write("\t<form action=\"myLoginmyController\" method=\"post\" >\r\n");
      out.write("\t\t<input type=\"text\" value=\"Email\" name=\"RegNo\" onfocus=\"this.value = '';\" onblur=\"if (this.value == '') {this.value = 'Email';}\"/>\r\n");
      out.write("\t\t<input type=\"password\" value=\"Password:\" name=\"Password\" onfocus=\"this.value = '';\" onblur=\"if (this.value == '') {this.value = 'Password:';}\"/>\r\n");
      out.write("\t\t<input type=\"submit\" value=\"Login\" /><br/>\r\n");
      out.write("\t\t<a  href=\"forgotPass1.jsp\" >Forgot password ?</a>\r\n");
      out.write("                <a  href=\"index.html.jsp\" >Register here</a>\r\n");
      out.write("                \r\n");
      out.write("\t</form>\r\n");
      out.write("\t\r\n");
      out.write("</div>\r\n");
      out.write("<!--login form end here-->\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
