package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class loginme_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("<!DOCTYPE HTML>\n");
      out.write("<html>\n");
      out.write("<head>\n");
      out.write("<title>User Login</title>\n");
      out.write("<!-- Custom Theme files-->\n");
      out.write("<link href=\"assets/css/styleL.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\"/>\n");
      out.write("<!-- Custom Theme files -->\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n");
      out.write("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /> \n");
      out.write("<meta name=\"keywords\" content=\"Ensaluto Form Responsive,Login form web template, Sign up Web Templates, Flat Web Templates, Login signup Responsive web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design\" />\n");
      out.write("<!--Google Fonts-->\n");
      out.write("<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>\n");
      out.write("<!--Google Fonts--->\n");
      out.write("<script src=\"js/jquery.min.js\"></script>\n");
      out.write(" \n");
      out.write(" <style type=\"text/css\">\n");
      out.write("\n");
      out.write(".customy {\n");
      out.write("    background-image: url(myassets/images/w5.jpg);\n");
      out.write("    background-size: contain;\n");
      out.write("    \n");
      out.write("\t\n");
      out.write("\tcolor: blue;\n");
      out.write("        text-decoration: darkmagenta;\n");
      out.write("\tfont-size:20px;\n");
      out.write("}\n");
      out.write("</style>\n");
      out.write(" \n");
      out.write("</head>\n");
      out.write("<body class=\"customy\">\n");
      out.write("<!--log in form start here-->\n");
      out.write("\n");
      out.write("<div class=\"app\">\n");
      out.write("    <div class=\"customy\">\n");
      out.write("\t<div class=\"top-bar\">\n");
      out.write("\t\t\t<div class=\"navg\">\n");
      out.write("\t\t\t\t\n");
      out.write("\t\t\t\n");
      out.write("\t\t\t\t<script>\n");
      out.write("                                  $( \"span.menu\").click(function() {\n");
      out.write("                                                                    $(  \"ul.res\" ).slideToggle(\"slow\", function() {\n");
      out.write("                                                                     // Animation complete.\n");
      out.write("                                                                     });\n");
      out.write("                                                                     });\n");
      out.write("\t\t       </script>\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t<h2 align=\"center\">User login</h2>\n");
      out.write("                \n");
      out.write("\t</div>\n");
      out.write("    <div class=\"forgotpwd\">\n");
      out.write("\t\t\t<!-- code to display error message  -->\n");
      out.write("\t\t\t");
if(request.getParameter("msg")!=null){
      out.write("\n");
      out.write("\t\t\t\t<h4 style=\"color:red\">");
      out.print(request.getParameter("msg") );
      out.write("</h4>\n");
      out.write("                                 \n");
      out.write("\t\t\t");
}else{
      out.write("\n");
      out.write("                        <h3>Please Put in your Login Details.</h3>\n");
      out.write("                               ");
 }
                        
      out.write("\n");
      out.write("\t\t\t<!-- error message code ends here -->\n");
      out.write("\t\t</div>\n");
      out.write("\t<div class=\"cam-img\">\n");
      out.write("\t\t \n");
      out.write("\t</div>\n");
      out.write("\t<form action=\"myLoginmyController\" method=\"post\" >\n");
      out.write("\t\t<input type=\"text\" value=\"Email\" name=\"RegNo\" onfocus=\"this.value = '';\" onblur=\"if (this.value == '') {this.value = 'Email';}\"/>\n");
      out.write("\t\t<input type=\"password\" value=\"Password:\" name=\"Password\" onfocus=\"this.value = '';\" onblur=\"if (this.value == '') {this.value = 'Password:';}\"/>\n");
      out.write("\t\t<input type=\"submit\" value=\"Login\" /><br/>\n");
      out.write("                <h2>Forgot password? click<a  href=\"forgotPass1.jsp\" >here</a></h2>\n");
      out.write("                <h2>If you need to register, please click<a  href=\"index.html\" >here</a></h2>\n");
      out.write("                \n");
      out.write("\t</form>\n");
      out.write("    </div>\n");
      out.write("</div>\n");
      out.write("<!--login form end here-->\n");
      out.write("</body>\n");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "layout/Footer.jsp", out, false);
      out.write("\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
