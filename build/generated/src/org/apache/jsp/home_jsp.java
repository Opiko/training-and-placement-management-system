package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.Date;

public final class home_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Training and Placement Management System</title>\n");
      out.write("        <link href=\"style1.css\" rel='stylesheet' type='text/css'>\n");
      out.write("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <nav>\n");
      out.write("            <header id=\"image\">\n");
      out.write("                <img src=\"assets/img/wi.jpg\" alt=\"Our Logo\" style=\"float:left\" width=\"200px\" height=\"123px\">\n");
      out.write("                <img src=\"assets/img/wi.jpg\" alt=\"Our Logo\" style=\"float:right\" width=\"200px\" height=\"123px\">\n");
      out.write("            <p><b>Training and Placement Management System</b><p>\n");
      out.write("            </header>\n");
      out.write("        <div id=\"heading\">\n");
      out.write("            &nbsp;&nbsp;<a href=\"home.jsp\" title=\"Home Page\">HOME</a>&nbsp;&nbsp;\n");
      out.write("        <a href=\"rooms.jsp\" title=\"Available Rooms Page\">AVAILABLE ROOMS</a>&nbsp;&nbsp;\n");
      out.write("        <a href=\"index.jsp\" title=\"Book Rooms Page\">BOOK ROOMS</a>&nbsp;&nbsp;\n");
      out.write("        <a href=\"index.jsp\" title=\"Guest Login Page\">GUEST</a>&nbsp;&nbsp;\n");
      out.write("        <a href=\"about.jsp\" title=\"About Us Page\">ABOUT US</a>&nbsp;&nbsp;\n");
      out.write("        <a href=\"contacts.jsp\" title=\"Contacts Page\">CONTACTS</a>&nbsp;&nbsp;\n");
      out.write("        <a href=\"RoomOperatorLogin.jsp\" title=\"Staff Login Page\">STAFF</a>\n");
      out.write("        </div>\n");
      out.write("            </nav>\n");
      out.write("    <marquee behavior=\"alternate\"><a href=\"about.jsp\"><b>WE HELP YOU FIND YOUR DREAM JOB</b></a></marquee>\n");
      out.write("           \n");
      out.write("           <div class=\"slideshow-containe\">\n");
      out.write("\n");
      out.write("<div class=\"mySlides fade\">\n");
      out.write("  <div class=\"numbertext\">1 / 3</div>\n");
      out.write("  <img src=\"image/arc01_1.jpg\" style=\"width:100%; height: 580px\">\n");
      out.write("  <div class=\"text\">Caption Text</div>\n");
      out.write("</div>\n");
      out.write("\n");
      out.write("<div class=\"mySlides fade\">\n");
      out.write("  <div class=\"numbertext\">2 / 3</div>\n");
      out.write("  <img src=\"image/arc02_1.jpg\" style=\"width:100%; height: 580px\">\n");
      out.write("  <div class=\"text\">Caption Two</div>\n");
      out.write("</div>\n");
      out.write("\n");
      out.write("<div class=\"mySlides fade\">\n");
      out.write("  <div class=\"numbertext\">3 / 3</div>\n");
      out.write("  <img src=\"image/arc03_1.jpg\" style=\"width:100%; height: 580px\">\n");
      out.write("  <div class=\"text\">Caption Three</div>\n");
      out.write("</div>\n");
      out.write("\n");
      out.write("<a class=\"prev\" onclick=\"plusSlides(-1)\">&#10094;</a>\n");
      out.write("<a class=\"next\" onclick=\"plusSlides(1)\">&#10095;</a>\n");
      out.write("\n");
      out.write("</div>\n");
      out.write("<br>\n");
      out.write("\n");
      out.write("<div style=\"text-align:center\">\n");
      out.write("  <span class=\"dot\" onclick=\"currentSlide(1)\"></span> \n");
      out.write("  <span class=\"dot\" onclick=\"currentSlide(2)\"></span> \n");
      out.write("  <span class=\"dot\" onclick=\"currentSlide(3)\"></span> \n");
      out.write("</div>\n");
      out.write("\n");
      out.write("<script>\n");
      out.write("var slideIndex = 1;\n");
      out.write("showSlides(slideIndex);\n");
      out.write("\n");
      out.write("function plusSlides(n) {\n");
      out.write("  showSlides(slideIndex += n);\n");
      out.write("}\n");
      out.write("\n");
      out.write("function currentSlide(n) {\n");
      out.write("  showSlides(slideIndex = n);\n");
      out.write("}\n");
      out.write("\n");
      out.write("function showSlides(n) {\n");
      out.write("  var i;\n");
      out.write("  var slides = document.getElementsByClassName(\"mySlides\");\n");
      out.write("  var dots = document.getElementsByClassName(\"dot\");\n");
      out.write("  if (n > slides.length) {slideIndex = 1}    \n");
      out.write("  if (n < 1) {slideIndex = slides.length}\n");
      out.write("  for (i = 0; i < slides.length; i++) {\n");
      out.write("      slides[i].style.display = \"none\";  \n");
      out.write("  }\n");
      out.write("  for (i = 0; i < dots.length; i++) {\n");
      out.write("      dots[i].className = dots[i].className.replace(\" active\", \"\");\n");
      out.write("  }\n");
      out.write("  slides[slideIndex-1].style.display = \"block\";  \n");
      out.write("  dots[slideIndex-1].className += \" active\";\n");
      out.write("}\n");
      out.write("</script>    \n");
      out.write("       <div class=\"row\">  \n");
      out.write("  <div class=\"leftcolumn\">\n");
      out.write("    <div class=\"card\">\n");
      out.write("      <center><h1><b><marquees>Training and Placement Management System</marquees></b></h1>\n");
      out.write("      <h3>We help you find your dream job or recruits</h3></center>\n");
      out.write("      <img src=\"image/arc04_1.jpg\" class=\"fakeimg\" style=\"height:500px;\">ARC Hotel\n");
      out.write("      <p><a href=\"#\">Wanna Know more? Click Here...</a></p>\n");
      out.write("      <p>qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq</p>\n");
      out.write("    </div>\n");
      out.write("    <div class=\"card\">\n");
      out.write("      <h2>wwwwwwwwwwwwww </h2>\n");
      out.write("      <h5>All you need is here</h5>\n");
      out.write("      <img src=\"image/arc.jpg\" alt=\"ARC image\" class=\"fakeimg\" style=\"height:500px;\">\n");
      out.write("      <p><a href=\"#\">Full Article? Click Here...</a></p>\n");
      out.write("      <p>qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq</p>\n");
      out.write("    </div>\n");
      out.write("  </div>\n");
      out.write("  <div class=\"rightcolumn\">\n");
      out.write("    <div class=\"card\">\n");
      out.write("      <h2>About Arc Hotel </h2>\n");
      out.write("      <img src=\"assets/img/wi.jpg\" alt=\"Our Image\" class=\"fakeimg\" style=\"height:100px;\">\n");
      out.write("      <p><a href=\"#\">Full Article About Us? Click Here...</a></p>\n");
      out.write("    </div>\n");
      out.write("    <div class=\"card\">\n");
      out.write("      <h3>Popular Sites In The Hotel</h3>\n");
      out.write("      <img src=\"assets/img/wi.jpg\" class=\"fakeimg\"><p></p>\n");
      out.write("      <img src=\"assets/img/wi.jpg\" class=\"fakeimg\"><p></p>\n");
      out.write("      <img src=\"assets/img/wi.jpg\" class=\"fakeimg\"><p></p>\n");
      out.write("    </div>\n");
      out.write("    <div class=\"card\">\n");
      out.write("      <h3>Follow Us On Twitter Or Facebook</h3>\n");
      out.write("      <p><a href=\"http://www.twitter.com\">Twitter</a><br><a href=\"http://www.facebook.com\">Facebook</a></p>\n");
      out.write("    </div>\n");
      out.write("  </div>\n");
      out.write("</div>\n");
      out.write("<section>     \n");
      out.write("    <div class=\"car\">\n");
      out.write("      <h2>qqqqqqqqqq </h2>\n");
      out.write("      <h5>All you need is here</h5>\n");
      out.write("      <img src=\"assets/img/wi.jpgg\" alt=\"ARC image\" class=\"fakeimg\" style=\"height:400px; width:100%\">\n");
      out.write("      <p><a href=\"#\">Full Article? Click Here...</a></p>\n");
      out.write("      <p>qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq</p>\n");
      out.write("    </div>\n");
      out.write("<script>\n");
      out.write("var slideIndex = 0;\n");
      out.write("showSlides();\n");
      out.write("\n");
      out.write("function showSlides() {\n");
      out.write("    var i;\n");
      out.write("    var slides = document.getElementsByClassName(\"mySlides\");\n");
      out.write("    var dots = document.getElementsByClassName(\"dot\");\n");
      out.write("    for (i = 0; i < slides.length; i++) {\n");
      out.write("       slides[i].style.display = \"none\";  \n");
      out.write("    }\n");
      out.write("    slideIndex++;\n");
      out.write("    if (slideIndex > slides.length) {slideIndex = 1}    \n");
      out.write("    for (i = 0; i < dots.length; i++) {\n");
      out.write("        dots[i].className = dots[i].className.replace(\" active\", \"\");\n");
      out.write("    }\n");
      out.write("    slides[slideIndex-1].style.display = \"block\";  \n");
      out.write("    dots[slideIndex-1].className += \" active\";\n");
      out.write("    setTimeout(showSlides, 5000); // Change image every 2 seconds\n");
      out.write("}\n");
      out.write("</script>\n");
      out.write("</section>\n");
      out.write("<aside>\n");
      out.write("    <h2>This displays an advert for the hotel's management</h2>\n");
      out.write("    <div style=\"text-align:center\"> \n");
      out.write("  <button onclick=\"playPause()\">Play/Pause</button> \n");
      out.write("  <button onclick=\"makeBig()\">Big</button>\n");
      out.write("  <button onclick=\"makeSmall()\">Small</button>\n");
      out.write("  <button onclick=\"makeNormal()\">Normal</button>\n");
      out.write("  <br><br>\n");
      out.write("    <video id=\"video1\" width=\"280\" controls>\n");
      out.write("  <source src=\"image/mov.mp4\" type=\"video/mp4\">\n");
      out.write("  <source src=\"image/mov.ogg\" type=\"video/ogg\">\n");
      out.write("</video>\n");
      out.write("  </div>\n");
      out.write("  <script> \n");
      out.write("var myVideo = document.getElementById(\"video1\"); \n");
      out.write("\n");
      out.write("function playPause() { \n");
      out.write("    if (myVideo.paused) \n");
      out.write("        myVideo.play(); \n");
      out.write("    else \n");
      out.write("        myVideo.pause(); \n");
      out.write("} \n");
      out.write("\n");
      out.write("function makeBig() { \n");
      out.write("    myVideo.width = 550; \n");
      out.write("} \n");
      out.write("\n");
      out.write("function makeSmall() { \n");
      out.write("    myVideo.width = 200; \n");
      out.write("} \n");
      out.write("\n");
      out.write("function makeNormal() { \n");
      out.write("    myVideo.width = 470; \n");
      out.write("} \n");
      out.write("</script>\n");
      out.write("<p>\n");
      out.write("Video courtesy of \n");
      out.write("<a href=\"http://www.youtube.com\" target=\"_blank\">ARC Management Video</a>.\n");
      out.write("</p>\n");
      out.write("  <center><h5>Find Us On</h5>\n");
      out.write("  <div class=\"w3-xlarge w3-padding-16\">\n");
      out.write("    <i class=\"fa fa-facebook-official w3-hover-opacity\"></i>\n");
      out.write("    <i class=\"fa fa-instagram w3-hover-opacity\"></i>\n");
      out.write("    <i class=\"fa fa-snapchat w3-hover-opacity\"></i>\n");
      out.write("    <i class=\"fa fa-pinterest-p w3-hover-opacity\"></i>\n");
      out.write("    <i class=\"fa fa-twitter w3-hover-opacity\"></i>\n");
      out.write("    <i class=\"fa fa-linkedin w3-hover-opacity\"></i>\n");
      out.write("  </div>\n");
      out.write("  </center>\n");
      out.write(" </aside> <center><p><div id=\"footer\"><footer>Copyright &copy;");
      out.print(new Date());
      out.write("@ARCHotel.co.ke </p></footer></div></center>\n");
      out.write("\n");
      out.write("</body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
