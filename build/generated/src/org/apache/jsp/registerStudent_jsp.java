package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Connection;
import libraryy.connection;

public final class registerStudent_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "layout/StudentHeader.jsp", out, false);
      out.write("\n");
      out.write("<script type=\"text/javascript\" src=\"assets/js/blank.js\"></script>\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Register Student</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <div class=\"mymainWrapper col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n");
      out.write("\t<div class=\"centerformWrapper col-lg-4 col-md-6 col-sm-12 col-xs-12\">\n");
      out.write("\t\t<div class=\"centertitle row col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n");
      out.write("\t\t\tStudent Registration form\n");
      out.write("                </div>\n");
      out.write("\t\t<div class=\"errordiv col-lg-10 col-md-10 col-sm-10 col-xs-10\">\n");
      out.write("\t\t");

			String msg= request.getParameter("msg");
			if(msg!=null){
				out.print(msg);
			}else{
      out.write("\n");
      out.write("                        <h4>Please Put in your Correct Details.</h4>\n");
      out.write("                               ");
 }
                 try{
                 connection conn=new connection();
                // connection conn1=new connection();

                    Connection con;
                    //Connection con1;
                    
                    con=conn.connect();
                   //con1 = conn1.connect();

             
             String query="select * from Courses";
             //String query1="SELECT Cid,DISTINCT Faculty FROM courses";

             Statement st=con.createStatement();
            //Statement st1=con1.createStatement();
             
             ResultSet rst =null;
            // ResultSet rst1 =null;

                rst=st.executeQuery(query);
               // rst=st1.executeQuery(query1);
               
                      
		
      out.write("\n");
      out.write("\t\t</div>\n");
      out.write("                <div class=\"centerform col-lg-12 col-md-12 col-sm-12c col-xs-12\">\n");
      out.write("\t\t<form action=\"studentRegisterController\"\n");
      out.write("                      onsubmit=\"return validateLogin15();\" method=\"post\">\n");
      out.write("\t\t\t\t\n");
      out.write("\t\t\t\n");
      out.write("\t\t\t\t<div class=\"form-group\">\n");
      out.write("\t\t\t\t\t<label for=\"Fname\">First Name</label> \n");
      out.write("\t\t\t\t\t<input type=\"text\" name=\"Fname\" class=\"form-control required\"/>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("                   \n");
      out.write("                    <div class=\"form-group\">\n");
      out.write("\t\t\t\t\t<label for=\"Lname\">Last Name</label> \n");
      out.write("\t\t\t\t\t<input type=\"text\" name=\"Lname\" class=\"form-control required\"/>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("                     \n");
      out.write("                   \n");
      out.write("                    \n");
      out.write("                     <div class=\"form-group\">\n");
      out.write("\t\t\t\t\t<label for=\"email\">Phone Number</label> \n");
      out.write("\t\t\t\t\t<input type=\"text\" name=\"Phone\" class=\"form-control required \"/>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("                    <div class=\"\">\n");
      out.write("\t\t\t\t\t<label for=\"Phone\">Your Email Address</label> \n");
      out.write("\t\t\t\t\t<input type=\"text\" placeholder=\"xyz@gmail.com\" name=\"Email\"  class=\"form-control required\"/>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("                    \t\t\n");
      out.write("                    <div class=\"form-group\">\n");
      out.write("\t\t\t\t\t<label for=\"DoB\">Age</label> \n");
      out.write("\t\t\t\t\t<input class=\"form-control\" placeholder=\"Above 18yrs\"  min=\"18\" max= \"35\" type=\"number\" name=\"DoB\" required />\n");
      out.write("\t\t\t\t</div>\n");
      out.write("                    \n");
      out.write("                     <div class=\"form-group\">\n");
      out.write("\t\t\t\t\t<label for=\"gender\">Select your gender.</label>\n");
      out.write("\t\t\t\t\t<select name=\"gender\" id=\"product\" class=\"form-control\">\n");
      out.write("\t\t\t\t\t\t<option value=\"Male\">Male</option>\n");
      out.write("\t\t\t\t\t\t<option value=\"Female\">Female</option>\t\n");
      out.write("\t\t\t\t\t</select>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("                   \n");
      out.write("                    <div class=\"form-group\">\n");
      out.write("\t\t\t\t\t<label for=\"level\">Which level of Study?</label>\n");
      out.write("\t\t\t\t\t<select name=\"level\" id=\"product\" class=\"form-control\">\n");
      out.write("\t\t\t\t\t\t<option value=\"Student\">Degree</option>\n");
      out.write("\t\t\t\t\t\t<option value=\"Student\">Diploma</option>\n");
      out.write("                                                <option value=\"Student\">Certificate</option>\n");
      out.write("\t\t\t\t\t</select>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("                    ");
      out.write("\n");
      out.write("                    \n");
      out.write("                    <div class=\"form-group\">\n");
      out.write("\t\t\t\t\t<label for=\"course\">select your course of study</label>\n");
      out.write("                                        \n");
      out.write("\t\t\t\t\t<select name=\"course\" id=\"product\" class=\"form-control\">\n");
      out.write("                                            ");

                                        while(rst.next())
                                        {
                                        
      out.write("\n");
      out.write("                                            <option value=\"");
      out.print(rst.getInt("Cid"));
      out.write('"');
      out.write('>');
      out.print(rst.getString("Cname"));
      out.write("</option>\n");
      out.write("\t\t\t\t\t\t");

                                                }}catch(Exception e){
                                                       e.printStackTrace();
                                            }
                                                
      out.write("\n");
      out.write("\t\t\t\t\t</select>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("                    <div class=\"form-group\">\n");
      out.write("\t\t\t\t\t<label for=\"school\">select your school</label>\n");
      out.write("\t\t\t\t\t<select name=\"school\" id=\"product\" class=\"form-control\">\n");
      out.write("\t\t\t\t\t\t<option value=\"Student\">Egerton</option>\n");
      out.write("\t\t\t\t\t\t<option value=\"Student\">KU</option>\n");
      out.write("                                                <option value=\"Student\">UoN</option>\n");
      out.write("\t\t\t\t\t</select>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("                    \n");
      out.write("                    \n");
      out.write("                   <div class=\"form-group\">\n");
      out.write("\t\t\t\t\t<label for=\"VPassword\">Enter Password</label> \n");
      out.write("\t\t\t\t\t<input type=\"password\" name=\"Pswd\"  pattern=\"(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}\" title=\"Must contain at least one number and one uppercase and lowercase letter and at least 8 or more characters\" placeholder=\"Enter a minimum of 8 characters\" class=\"form-control\"/>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("                <div class=\"form-group\">\n");
      out.write("\t\t\t\t\t<label for=\"VPassword\">confirm Password</label> \n");
      out.write("\t\t\t\t\t<input type=\"password\" name=\"VPswd\"  pattern=\"(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}\" title=\"Must contain at least one number and one uppercase and lowercase letter and at least 8 or more characters\" placeholder=\"Must be the same as the above\" class=\"form-control\"/>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("                     \n");
      out.write("                 <div class=\"form-group\">\n");
      out.write("\t\t\t\t\t<label for=\"Sq\">Select Security Question.</label>\n");
      out.write("\t\t\t\t\t<select name=\"sq\" id=\"product\" class=\"form-control\">\n");
      out.write("\t\t\t\t\t\t<option value=\"Which town are you from?\">Which town are you from?</option>\n");
      out.write("\t\t\t\t\t\t<option value=\"Name your Primary school\">Name your Primary school</option>\n");
      out.write("                                                <option value=\"Which place do you like to visit?\">Which place do you like to visit?</option>\n");
      out.write("\t\t\t\t\t</select>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("                <div class=\"form-group\">\n");
      out.write("\t\t\t\t\t<label for=\"SA\">Security answer</label> \n");
      out.write("\t\t\t\t\t<input type=\"text\" name=\"sa\" class=\"form-control\"/>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("                  \n");
      out.write("                    \n");
      out.write("                    <div class=\"form-group\">\n");
      out.write("                            \n");
      out.write("                                <button type=\"reset\" class=\"btn btn-danger\">Reset</button>\n");
      out.write("\t\t\t\t\t<button type=\"submit\" class=\"btn btn-danger\">Submit</button>\n");
      out.write("                                        \n");
      out.write("                                    \n");
      out.write("\t\t\t\t\t\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t</form>\n");
      out.write("                    \t</div>\n");
      out.write("\t</div>\n");
      out.write("</div>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
