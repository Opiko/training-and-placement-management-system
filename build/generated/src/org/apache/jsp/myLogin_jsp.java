package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.ResultSet;

public final class myLogin_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!-- ***********   START   *********** -->\n");
      out.write("\n");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "layout/Header.jsp", out, false);
      out.write("\n");
      out.write("<script type=\"text/javascript\" src=\"assets/js/blank.js\"></script>\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("       \n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <style type=\"text/css\">\n");
      out.write(".customy {\n");
      out.write("    background-image: background-image: url('assets/img/r03.jpg');\n");
      out.write("\tfont-family: Courier;\n");
      out.write("\tcolor: blue;\n");
      out.write("        text-decoration: darkmagenta;\n");
      out.write("\tfont-size:20px;\n");
      out.write("}\n");
      out.write("</style>\n");
      out.write("<div class=\"mymainWrapper col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n");
      out.write("\t<div class=\"centerformWrapper col-lg-4 col-md-6 col-sm-12 col-xs-4\">\n");
      out.write("\t\t<div class=\"centertitle row col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n");
      out.write("\t\t\tLOGIN\n");
      out.write("\t\t</div>\n");
      out.write("            <div class=\"errordiv\">\n");
      out.write("\t\t\t<!-- code to display error message  -->\n");
      out.write("\t\t\t");
if(request.getParameter("msg")!=null){
      out.write("\n");
      out.write("\t\t\t\t<h4>");
      out.print(request.getParameter("msg") );
      out.write("</h4>\n");
      out.write("                                 \n");
      out.write("\t\t\t");
}else{
      out.write("\n");
      out.write("                        <h4>Please Put in your Login Details.</h4>\n");
      out.write("                               ");
 }
                        
      out.write("\n");
      out.write("\t\t\t<!-- error message code ends here -->\n");
      out.write("\t\t</div>\n");
      out.write("\t\t<div class=\"centerform col-lg-12 col-md-12 col-sm-12c col-xs-12\">\n");
      out.write("\t\t<form name=\"loginForm\" action=\"myLoginmyController\"\n");
      out.write("\t\t\t\tonsubmit=\"return validateLogin14();\" method=\"post\">\n");
      out.write("\t\t\t\n");
      out.write("\t\t\t\t<div class=\"form-group\">\n");
      out.write("\t\t\t\t\t<label for=\"username\">Username</label> \n");
      out.write("\t\t\t\t\t<input type=\"text\" name=\"RegNo\" class=\"form-control\"/>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t\t<div class=\"form-group\">\n");
      out.write("\t\t\t\t\t<label for=\"password\">Password</label> \n");
      out.write("\t\t\t\t\t<input type=\"password\" name=\"Password\" class=\"form-control\"/>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t\t<div class=\"form-group\">\n");
      out.write("\t\t\t\t\t<button type=\"submit\" class=\"btn btn-success\">Login</button>\n");
      out.write("\t\t\t\t\t<button type=\"reset\" class=\"btn btn-danger\">Reset</button>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t</form>\t\n");
      out.write("\t\t</div>\n");
      out.write("\t</div>\n");
      out.write("     \n");
      out.write("</div>\n");
      out.write("  <div class=\"customy\" >\n");
      out.write("       <p> Register new student <a href=\"registerSudent.jsp\"> here </a></p>\n");
      out.write("        <p> Register new company <a href=\"registerCompany.jsp\"> here </a></p>\n");
      out.write("    </div>\n");
      out.write("<br>\n");
      out.write("<!-- ********************************* -->\n");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "layout/Footer.jsp", out, false);
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
