package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class uploadCV_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!-- ***********   START   *********** -->\n");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "layout/StudentHeader.jsp", out, false);
      out.write("\n");
      out.write("<script type=\"text/javascript\" src=\"assets/js/blank.js\"></script>\n");
      out.write("\n");
      out.write("<div class=\"mymainWrapper col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n");
      out.write("\t<div class=\"centerformWrapper col-lg-4 col-md-6 col-sm-12 col-xs-12\">\n");
      out.write("\t\t<div class=\"centertitle row col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n");
      out.write("\t\t\tSTUDENT REGISTRATION FORM\n");
      out.write("\t\t</div>\n");
      out.write("\t\t<div class=\"errordiv col-lg-10 col-md-10 col-sm-10 col-xs-10\">\n");
      out.write("\t\t");

			String msg= request.getParameter("msg");
			if(msg!=null){
				out.print(msg);
			}
		
      out.write("\n");
      out.write("\t\t</div>\n");
      out.write("\t\t<div class=\"centerform col-lg-12 col-md-12 col-sm-12c col-xs-12\">\n");
      out.write("\t\t<form name=\"register\" action=\"myRegisterController\" method=\"post\">\n");
      out.write("\t\t\t\t\n");
      out.write("\t\t\t\n");
      out.write("\t\t\t\t<div class=\"form-group\">\n");
      out.write("\t\t\t\t\t<label for=\"fname\">Upload your Photo</label> \n");
      out.write("\t\t\t\t\t<input type=\"file\" name=\"photo\" class=\"form-control\"/>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("                    <div class=\"form-group\">\n");
      out.write("\t\t\t                    <div class=\"form-group\">\n");
      out.write("\t\t<label for=\"lName\">Upload your CV</label> \n");
      out.write("\t\t\t\t\t<input type=\"file\" name=\"CV\" class=\"form-control\"/>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("                  \n");
      out.write("                   \n");
      out.write("\t\t\t\t<div class=\"form-group\">\n");
      out.write("                                    \n");
      out.write("                                    <button type=\"reset\" class=\"btn btn-danger\">Reset</button>\n");
      out.write("\t\t\t\t\t<button type=\"submit\" class=\"btn btn-danger\">Submit</button>\n");
      out.write("\t\t\t\t\t\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t</form>\n");
      out.write("                                        \n");
      out.write("\t\t</div>\n");
      out.write("         \n");
      out.write("\t</div>\n");
      out.write("          \n");
      out.write("</div>\n");
      out.write("\n");
      out.write("</body>\n");
      out.write("<!-- ********************************* -->\n");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "layout/Footer.jsp", out, false);
      out.write('\n');
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
