package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;

//import business.Validate;

@WebServlet("/LoginController")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		  //get the action from login request 
   String action = request.getParameter("action");
	RequestDispatcher rd=null;
	//check user's action 
	if (action.equals("login")){ 
	//get parameters(admin_ID and password) from the input form
	String UserName = request.getParameter("UserName");
    String password= request.getParameter("password");
    /*
    //call validation method and  do the comparison
	if(Validate.validateLogin(UserName, password)) {
		HttpSession session = request.getSession();
		session.setAttribute(" UserName ",  UserName );
		 session.setMaxInactiveInterval(5*60);
	 
   
if(res.next()){
       String type=res.getString("userType");
       if("admin".equals(type)){
          rd = request.getRequestDispatcher("/AdminPage.jsp");
	 rd.forward(request, response);
      } 
      else if("company".equals(type)){
          rd = request.getRequestDispatcher("/AdminPage.jsp");
	 rd.forward(request, response);
      }
       
}
else{
     rd = request.getRequestDispatcher("/AdminPage.jsp");
	 rd.forward(request, response);
}
         
         
	 }	
	else{
   //if no match found redirect to an error page
		 rd = request.getRequestDispatcher("/Admin_Login_Page.jsp");
		JOptionPane.showMessageDialog(null, "Login failed,this might be as a result of wrong username or password");
        rd.forward(request, response);
	}
	 }
	//staff authentication
	else if(action.equals("staff")){
	String staffID = request.getParameter("username");
	String pass = request.getParameter("password");	
	 if(Validate.checkStaff(staffID, pass)) {
	 HttpSession session = request.getSession();
	 session.setAttribute("staff", staffID);
	 session.setMaxInactiveInterval(5*60);
	  rd = request.getRequestDispatcher("/StaffPage.jsp");
	  request.setAttribute("staffID",staffID);
	  rd.forward(request, response);
	 
	  }
	else{
    //if no match found redirect to an error page
		rd = request.getRequestDispatcher("/LoginError.html");
		rd.forward(request, response);
	  }
	}
	
	 else if (action.equals("student")){ 
	   //get parameters(admin_ID and password) from the input form
	   String studentID = request.getParameter("student_Number");
	   String password = request.getParameter(" ");
	 //call validation method and  do the comparison
		if(Validate.checkStudent(studentID, password)) {
		HttpSession session = request.getSession();
		session.setAttribute("student", studentID);
		session.setMaxInactiveInterval(5*60);
		rd = request.getRequestDispatcher("/studentPage.jsp");
		rd.forward(request, response);	  
		    
		}
	 else {
		//if no match found redirect to an error page
		rd = request.getRequestDispatcher("/StudentLoginPage.jsp");
		JOptionPane.showMessageDialog(null, "Wrong Student Number or Password");
        rd.forward(request, response);
	 }
*/
	}

	}
}
