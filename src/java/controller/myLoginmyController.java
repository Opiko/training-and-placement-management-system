/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import helper.Validate;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import libraryy.connection;
public class myLoginmyController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
         String Email= request.getParameter("RegNo");
    String Password= request.getParameter("Password");
    // form validation
    
     
		String errorMessage = Validate.validateLogin(Email, Password);
		if (!errorMessage.equalsIgnoreCase("")) {
			
				response.sendRedirect("loginForm.jsp?msg="+errorMessage);
			
			
		}
                else{
                    
    connection conn=new connection();
    Connection con;
             con = conn.connect();
  //String query="Select userType from user where userName = ? and password = ? ";
 String query="select * from users1  WHERE Email=? and Password=?";
    
  
    PreparedStatement pst=con.prepareStatement(query);
    pst.setString(1, Email);
     pst.setString(2, Password);
     ResultSet rs=pst.executeQuery();
 
     
     if(rs.next()){
         String role=rs.getString("Role");
         HttpSession session = request.getSession();
		 if(session!=null){
			// session.setAttribute("Email",Email );
     session.setAttribute("Role",rs.getString("Role"));
         session.setAttribute("Fname",rs.getString("Fname"));
         session.setAttribute("Lname",rs.getString("Lname"));
         session.setAttribute("Email",rs.getString("Email"));
         session.setAttribute("phone",rs.getString("phone"));
         session.setAttribute("Dob",rs.getString("DoB"));
         session.setAttribute("gender",rs.getString("gender"));
         session.setAttribute("level",rs.getString("level"));
         session.setAttribute("course",rs.getString("course"));
         session.setAttribute("school",rs.getString("school"));
         session.setAttribute("GPA",rs.getString("GPA"));
         
          session.setAttribute("image",rs.getString("image"));
                 }
         
          if("Student".equals(role)){
         
               response.sendRedirect("student.jsp");
         }
          else if("Admin".equals(role)){
        
               response.sendRedirect("admin.jsp");
         }
          else if("Company".equals(role)){
        
               response.sendRedirect("company.jsp");
         }
         else{
              out.println("User not valid"); 
         }
       
     con.close();
     }
     else{
         String error="Please put in correct email and password";
         response.sendRedirect("loginForm.jsp?msg="+error);
     }
    
    }
        } catch (SQLException ex) {
            Logger.getLogger(myLoginmyController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
