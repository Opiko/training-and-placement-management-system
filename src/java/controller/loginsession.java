/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.security.NoSuchAlgorithmException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.UserModel;


/**
 *
 * @author Davey
 */
@WebServlet(name = "loginsession", urlPatterns = {"/loginsession"})
public class loginsession extends HttpServlet {

   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String uName=request.getParameter("RegNo");
		 String password1=request.getParameter("password");
		
		try {
			
		
		
		 HttpSession session = request.getSession(false);
		 if(session!=null)
			 session.setAttribute("Email",uName );
		     UserModel user = new  UserModel();
		 
			libraryy.connection conn=new libraryy.connection();
    java.sql.Connection con;
             con = conn.connect();
			Statement statement = (Statement) con.createStatement();
			String retrieve="select * from users1  WHERE Email=? and Password=?";
			ResultSet res = statement.executeQuery(retrieve);
			if(res.next()){
				
                                    
				
				String Fname=res.getString("Fname");
				session.setAttribute("Fname",Fname );
                                
                                
				String Lname=res.getString("Lname");
				session.setAttribute("Lname",Lname );
				
				
			
				String Email=res.getString("Email");
				session.setAttribute("Email",Email );
			    
				String password=res.getString("Password");
				session.setAttribute("Password",password);
                                
                                String role=res.getString("Role");
				session.setAttribute("Role",role);
			
			    session.setAttribute("User", user);
			    
                               
         
          if("Student".equals(role)){
         
               response.sendRedirect("student.jsp");
         }
          else if("Admin".equals(role)){
        
               response.sendRedirect("admin.jsp");
         }
          else if("Company".equals(role)){
        
               response.sendRedirect("company.jsp");
         }
         else{
              out.println("<p style=\"color:red;text-align:center;font-size:20px;line-height:20px;\">Sorry username or password error</p>");
			 RequestDispatcher rd=request.getRequestDispatcher("myLogin.jsp");
			rd.include(request,response);
         }
		
		 
		    
		 }  }
		 catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	} 
    }

   
