/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import model.studentModel;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
/**
 *
 * @author Davey
 */
public class searchController extends HttpServlet {

  
  
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
       String Name = request.getParameter("Name");
     
       
  ResultSet rs=null;
  if(Name!=null){
      studentModel mod=new studentModel();
      mod.setFname(Name);
      mod.setLname(Name);
      mod.searchSudentsFromDb();
      
      try{
          rs.last();
				int count = 0;
				count = rs.getRow();
				if (count <= 0) {
                                    String msg="No Student with the name" + Name +". Sorry for that!";
                                    response.sendRedirect("deleteStudent.jsp?msg=" + msg);
                                }
                                else {
					rs.beforeFirst();
					RequestDispatcher disp = request
							.getRequestDispatcher("deleteStudent.jsp");
					request.setAttribute("Name", request.getParameter("Name"));
					request.setAttribute("StudentInfo", rs);
					System.out.println("Search results found!!");
					disp.forward(request, response);
				}
      }catch (Exception e) {
          String msg="try again please an Error occured";
          response.sendRedirect("deleteStudent.jsp?msg=" + msg);
      }
  }
       
      
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(searchController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(searchController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
