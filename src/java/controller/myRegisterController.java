/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;

import static java.lang.Integer.parseInt;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import libraryy.connection;
import model.myUsermodel;
import model.studentModel;
/**
 *
 * @author Davey
 */
@WebServlet(name = "myRegisterController", urlPatterns = {"/myRegisterController"})
public class myRegisterController extends HttpServlet {

   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
         //retriving all parameters from register form
                String Fname = request.getParameter("Fname");
                String Lname = request.getParameter("Lname");
               int DoB =parseInt(request.getParameter("DoB"));
              int Phone =parseInt(request.getParameter("Phone"));
                String RegNo = request.getParameter("RegNo");
                String gender = request.getParameter("gender");
                String level = request.getParameter("level");
                String Resident = request.getParameter("Resident");
                String email = request.getParameter("email");
                String password = request.getParameter("password");
                String vpassword = request.getParameter("vpassword");
                String sq = request.getParameter("sq");
                String sa = request.getParameter("sa");
                
                
               //setting all the values in model class
                studentModel stm=new studentModel();
                
                stm.setFname(Fname);
                stm.setLname(Lname);
               // stm.setDoB(DoB);
                stm.setPhone(Phone);
                stm.setRegNo(RegNo);
                stm.setGender(gender);
                stm.setLevel(level);
                ;
                stm.setEmail(email);
                stm.setPassword(password);
                stm.setVPassword(vpassword);
                stm.setSq(sq);
                stm.setSa(sa);
                
                 String sqlc="insert into students values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
                int k= connection.registerStudent(stm, sqlc);
                String message="Data inserted";
               if(k!=0){
                   response.sendRedirect("loginForm.jsp?msg="+message);
                   
                   
               }
               else{
                   String message1="Data Not inserted";
                    response.sendRedirect("loginForm.jsp?msg="+message1);
               }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
