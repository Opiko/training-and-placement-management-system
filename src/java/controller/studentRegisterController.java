/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import helper.Validate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpSession;
import model.studentModel;


@WebServlet(name = "studentRegisterController", urlPatterns = {"/studentRegisterController"})
public class studentRegisterController extends HttpServlet {

   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, ParseException {
        
            response.setContentType("text/html;charset=UTF-8");
            //retriving all parameters from register form
            String Fname = request.getParameter("Fname");
            
            String Lname = request.getParameter("Lname");
            
         
            String Email = request.getParameter("Email");
            
            int phone =parseInt(request.getParameter("Phone"));
            
           int DoB =parseInt(request.getParameter("DoB"));
            String gender = request.getParameter("gender");
            
            String level = request.getParameter("level");
            String course = request.getParameter("course");
            
            String school = request.getParameter("school");
            
            String Password = request.getParameter("Pswd");
            String VPassword = request.getParameter("VPswd");
            String sq = request.getParameter("sq");
            String sa = request.getParameter("sa");
            
            //int graduation_Year =parseInt(request.getParameter("graduation_Year"));
            //String Award = request.getParameter("Award");
            
           
//SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//Date myDoB = sdf.parse(DoB); // you will get date here

 
            
            
         
            
            //setting all the values in model class
            studentModel stm=new studentModel();
            
            stm.setFname(Fname);
            stm.setLname(Lname);
           stm.setLname(course);
           stm.setLname(school);
            stm.setEmail(Email);
            stm.setPhone(phone);
            stm.setDoB(DoB);
            stm.setGender(gender);
            stm.setLevel(level);
            stm.setPassword(Password);
            stm.setVPassword(VPassword);
            stm.setSa(sq);
            stm.setSa(sa );
            
            // stm.setGraduation_Year(graduation_Year);
            //stm.setAward(Award);
            

           /*
            myUserModel1 usm1=new myUserModel1();
                 usm1.setFname(Fname);
                  usm1.setLname(Lname);
                   usm1.setRegNo(RegNo);
                    usm1.setPassword(Password);
                     usm1.setVPassword(VPassword);
                     
                      
                 String sql2="insert into users1 values(?,?,?,?,?)";
                 connection.RegisterUser(usm1,sql2);
         
           */
             HttpSession session = request.getSession();
		 if(session!=null)
			 session.setAttribute("Email",Email );
                 session.setAttribute("Lname",Lname );
                 
           String errorMessage = Validate.validateStudentProfile(Fname, Lname,Email);
		if (!errorMessage.equalsIgnoreCase("")) {
			
				response.sendRedirect("registerStudent.jsp?msg="+errorMessage);
			
			
		}
                else{
            try {
                     Class.forName("com.mysql.jdbc.Driver");
        Connection con= DriverManager.getConnection("jdbc:mysql://localhost:3306/training_placement", "root", "");
        Statement st=con.createStatement();
        
            
          
             st.executeUpdate("insert into mystudents(Fname,Lname,Email,phone,DoB,gender,level,course,school,Password,VPassword,sq,sa)"
                + "                          values('"+Fname+"','"+Lname+"','"+Email+"','"+phone+"','"+DoB+"','"+gender+"'"
                        + ",'"+level+"','"+course+"','"+school+"','"+Password+"','"+VPassword+"','"+sq+"','"+sa+"')");
            
             st.executeUpdate("insert into users1(Fname,Lname,Email,Password,VPassword)"
                + "                          values('"+Fname+"','"+Lname+"','"+Email+"','"+Password+"','"+VPassword+"')");
             
              String msg="Welcome Mr. "+Fname + " We are here to help you find your dream job";
             response.sendRedirect("student.jsp?msg="+msg);
             
             /*
            //String sql="insert into mystudents(Fname,Lname,RegNo,Email,phone,DoB,gender,level,Password,VPassword,sq,sa)"
              //      + "values(?,?,?,?,?,?,?,?,?,?,?,?)";
            
            int i=connection.registerStudent(stm,sql);
            String msg="You Have Registered";
            if(i!=0){
                response.sendRedirect("loginForm.jsp="+msg);
            }
            else{
                String msg1="You have not registered";
                response.sendRedirect("index.html="+msg1);
            }
             */
        } 
             catch (SQLException e) {
			System.out.println(Fname+"::registration not successful thrown by exception");
			String msg = "Registration Not successful";;
			response.sendRedirect("registerStudent.jsp?msg="+msg);
		} 
        }}
       // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(studentRegisterController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(studentRegisterController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(studentRegisterController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(studentRegisterController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
                  
    }



