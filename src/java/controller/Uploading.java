/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

/**
 *
 * @author Davey
 */

@MultipartConfig(maxFileSize = 10*1024*1024,maxRequestSize = 20*1024*1024,fileSizeThreshold = 5*1024*1024)

public class Uploading extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
 
        
        try (PrintWriter out = response.getWriter()) {
            
                  // HttpServletRequest httpServletRequest = (HttpServletRequest) request;
//String Email = (String) httpServletRequest.getSession(false).getAttribute("Email");
            
            InputStream input=null;
            InputStream input1=null;
            
            String Email=request.getParameter("Email");
            
            Part filePart=request.getPart("photo");
            Part filePart1=request.getPart("CV");
            
            if(filePart !=null || filePart1!=null){
                System.out.println(filePart.getName());
                System.out.println(filePart.getSize());
                System.out.println(filePart.getContentType());
                
                input=filePart.getInputStream();
                input1=filePart1.getInputStream();
                try{
						
				Connection con=null;	    
				//load connection drivers
				Class.forName("com.mysql.jdbc.Driver");
				//create connection with the database
				 con = DriverManager.getConnection("jdbc:mysql://localhost:3306/training_placement","root","");
				 
				String query="UPDATE mystudents SET mycv=?,image=? WHERE Email='"+Email+"' ";
                                
                                
				 PreparedStatement pst=con.prepareStatement(query);
                                 
                                 if(input !=null || input1 !=null){
                                     pst.setBlob(1, input);
                                     pst.setBlob(2,input1);
                                     
                                 }
                                 
                                 int row=pst.executeUpdate();
                                 if (row>0){
                                     String msg="File uploaded and saved";
                                     request.getRequestDispatcher("uploadCV.jsp?msg="+msg);
                                 }
                                 con.close();
                                
				}
                                        
				catch(Exception e){
				 e.printStackTrace();
                                 String err="Problem in try block";
                                request.getRequestDispatcher("findRecruit.jsp?msg="+err);
                                 out.println("Error!!!!!!!!!!!..... Cought Error");
                         
				}
                
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
 