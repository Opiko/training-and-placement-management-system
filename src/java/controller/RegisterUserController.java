package controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.UserModel;

/**
 * Servlet implementation class RegisterUserController
 */
public class RegisterUserController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RegisterUserController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String Fname = request.getParameter("Fname");
		String Lname = request.getParameter("Lname");
		String UserName = request.getParameter("UserName");
		String Email = request.getParameter("Email");
		String Phone = request.getParameter("Phone");
                String Role = request.getParameter("Role");
		String Resident = request.getParameter("Resident");
		String password = request.getParameter("password");
                String vpassword = request.getParameter("vpassword");
		String sq = request.getParameter("sq");
		String sa = request.getParameter("sa");
                
               
		
                if(vpassword.equals(password)){
                    
                
		UserModel um = new UserModel();
		um.setFname(Fname);
                um.setLname(Lname);
                um.setUserName(UserName);
                um.setEmail(Email);
                um.setPhone(Phone);
                um.setRole(Role);
                um.setResident(Resident);
		um.setPassword(password);
                um.setvpassword(vpassword);
		um.setsa(sa);
		um.setsq(sq);
		
		try {
                     Class.forName("com.mysql.jdbc.Driver");
        Connection con= DriverManager.getConnection("jdbc:mysql://localhost:3306/training_placement", "root", "");
        Statement st=con.createStatement();
        st.executeUpdate("insert into students(Fname,Lname,UserName,Email,Role,Phone,Resident,password,vpassword,sq,sa)"
                + "                          values('"+Fname+"','"+Lname+"'"
                + "                            "
                + "                      ,'"+UserName+"','"+Email+"','"+Role+"','"+Phone+"','"+Resident+"',"
                        + "'"+password+"','"+vpassword+"','"+sq+"','"+sa+"')");
        String msg1 = "Welcome Mr. "+Fname + "We are glad to work with your company";
        response.sendRedirect("loginForm.jsp?msg="+msg1);
                    
			if(um.insertRegistrationData()){
				System.out.println(Fname+"::registration successful\nAuto login into dashboard");
			
				/* ----- login into dash-board ------ */
			/*	Boolean login=false;
				try {
					login = um.selectLoginData1();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				/*if (login == true) {
					HttpSession session = request.getSession();
					session.setAttribute("email", UserName);
					session.setAttribute("type", "user");
					String userid = "";
					try {
						userid = um.getUserIDFromEmail(UserName);
					} catch (SQLException e) {
						e.printStackTrace();
					}
					session.setAttribute("userid", userid);
					System.out.println(userid + " : session set and logged in ");
					response.sendRedirect("indexUser.jsp");
				}
				/* ----- login complete ------ */
                        
                        
			}
                     
			else{
				System.out.println(Fname+"::registration not successful");
				String msg = "Welcome Mr. "+Fname + "We are glad to work with your company";
				response.sendRedirect("student.jsp?msg="+msg);
			}
		} catch (SQLException e) {
			//System.out.println(Fname+"::registration not successful thrown by exception");
			String msg = "Welcome Mr. "+Fname + "We are glad to work with your company";
			response.sendRedirect("student.jsp?msg="+msg);
		}   catch (ClassNotFoundException ex) {
                        Logger.getLogger(RegisterUserController.class.getName()).log(Level.SEVERE, null, ex);
                    }

	}
                else{
                    
                System.out.println(" Your Password " + password + " Does not match " + vpassword);
				String msg = "Registration not sucessful. Password do not match.";
				response.sendRedirect("RegisterUser.jsp?msg="+msg);
                }
        }
}
