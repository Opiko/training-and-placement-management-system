/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;
import libraryy.connection;
/**
 *
 * @author Davey
 */
@WebServlet(name = "NewServlet", urlPatterns = {"/NewServlet"})
public class NewServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String Email=request.getParameter("Email");
             String StdtEmail=request.getParameter("StdtEmail");
              String CompEmail=request.getParameter("CompEmail");
          String action=request.getParameter("action");
          
          String myEmail=request.getParameter("Email");
              String GPA=request.getParameter("gpa");
              
          RequestDispatcher rd=null;
          Connection con=null;
            
           
         
            
             if (action.equals("Delete")){
					
					
					try{
						
					    
				//load connection drivers
				Class.forName("com.mysql.jdbc.Driver");
				//create connection with the database
				 con = DriverManager.getConnection("jdbc:mysql://localhost:3306/training_placement","root","");
				 
                                
				 Statement st=con.createStatement();
                                 
				 st.executeUpdate("DELETE FROM mystudents  WHERE Email='"+Email+"'");
                                 st.executeUpdate("DELETE FROM users1  WHERE Email='"+Email+"'");
				 
				 
				
		       
		       String message="Student deleted successfully";
		         rd = request.getRequestDispatcher("deleteStudent.jsp?msg="+message);
				 rd.forward(request, response);
                                 
				con.close();
				}
				catch(Exception e){
				 e.printStackTrace();
                                 
                                 String emessage="System Error Occured. Request not completed! Try again. ";
		         rd = request.getRequestDispatcher("deleteStudent.jsp?msg="+emessage);
				 rd.forward(request, response);
                                 
                                 out.println("Error!!!!!!!!!!!..... Cought Error");
                         out.println("the passed value from form is "+Email);
                          out.println("the passed value from form is "+action);
				}
				}
           
           
            
            else if (action.equalsIgnoreCase("1Recruit111111"))
            {
					
					String compEmail = null;
                                        String compName = null;
					try{
						
					    
				//load connection drivers
				Class.forName("com.mysql.jdbc.Driver");
				//create connection with the database
				 con = DriverManager.getConnection("jdbc:mysql://localhost:3306/training_placement","root","");
				 
				String query="SELECT companies.CEmail AS compEmail, companies.Cname AS compName FROM companies WHERE CEmail='"+CompEmail+"'";
                                String query1="SELECT mystudents.Fname AS stFname,mystudents.Lname AS stLname,mystudents.Email AS stEmail FROM mystudents WHERE Email='"+StdtEmail+"' ";
                                
				 Statement st=con.createStatement();
                                 ResultSet rs=st.executeQuery(query);
                                 ResultSet rs1=st.executeQuery(query1);
                                 while(rs.next()){
                                     
                                     compEmail=(String)rs.getString("compEmail");
                                     compName=(String)rs.getString("compName");
                                 }
                                 rs.close();
                                 while(rs1.next()){
                                     String stFname=(String)rs1.getString("stFname");
                                     String stLname=(String)rs1.getString("stLname");
                                     String stEmail=(String)rs1.getString("stEmail");
                                     
                                     
                                     
                                      st.executeUpdate("INSERT INTO recruitment (CEmail, company_name,Fname,Lname,Student_email)"
                + "                          values('"+compEmail+"','"+compName+"','"+stFname+"','"+stLname+"','"+stEmail+"')");
                                 
                                 
                                 }
                                 rs1.close();
                               
                                 
				}
                                        
				catch(Exception e){
				 e.printStackTrace();
                                 String err="Problem in try block";
                                 rd = request.getRequestDispatcher("findRecruit.jsp?msg="+err);
                                 out.println("Error!!!!!!!!!!!..... Cought Error");
                         out.println("the passed value from form is "+CompEmail);
                          out.println("the passed value from form is "+action);
				}
				}
            
            
          
             else if (action.equalsIgnoreCase("Recruit"))
            {
					
					
					try{
						
					    
				//load connection drivers
				Class.forName("com.mysql.jdbc.Driver");
				//create connection with the database
				 con = DriverManager.getConnection("jdbc:mysql://localhost:3306/training_placement","root","");
				 
				String query="SELECT companies.Cname AS compName,mystudents.Fname AS stFname,mystudents.Lname AS stLname,"
                                        + "mystudents.Email AS stEmail FROM companies,mystudents WHERE companies.CEmail='"+CompEmail+"'"
                                        + " AND mystudents.Email='"+StdtEmail+"'";
                                
                                
				 Statement st=con.createStatement();
                                 Statement st1=con.createStatement();
                                 ResultSet rs=st.executeQuery(query);
                                 
                                 while(rs.next()){
                                     
                                     //String compEmail=(String)rs.getString("compEmail");
                                    String compName=(String)rs.getString("compName");
                                      String stFname=(String)rs.getString("stFname");
                                     String stLname=(String)rs.getString("stLname");
                                     //String stEmail=(String)rs.getString("stEmail");
                                
                                     
                                     
                                     
                                      st1.executeUpdate("INSERT INTO recruitment (CEmail, company_name,Fname,Lname,Student_email)"
                + "                          values('"+CompEmail+"','"+compName+"','"+stFname+"','"+stLname+"','"+StdtEmail+"')");
                                 
                                 
                                 }
                                 String msg="placement done successfully.";
                            rd = request.getRequestDispatcher("findRecruit.jsp?msg="+msg);
				 rd.forward(request, response);
                               
                                 
				}
                                        
				catch(Exception e){
				 e.printStackTrace();
                                 String err="Problem in try block";
                                 rd = request.getRequestDispatcher("findRecruit.jsp?msg="+err);
                                 out.println("Error!!!!!!!!!!!..... Cought Error");
                         out.println("the passed value from form is "+CompEmail);
                          out.println("the passed value from form is "+action);
				}
				}
             else if (action.equalsIgnoreCase("editGPA"))
            {
					
					
					try{
						
					    
				//load connection drivers
				Class.forName("com.mysql.jdbc.Driver");
				//create connection with the database
				 con = DriverManager.getConnection("jdbc:mysql://localhost:3306/training_placement","root","");
				 
				String query="UPDATE mystudents SET GPA ='"+GPA+"' WHERE Email ='"+myEmail+"'";
                                
                                Statement st=con.createStatement();
                                
				st.executeUpdate(query);
                                
                                     
                                     
                                     
                                  
                                 String msg="GPA edited successfully.";
                            rd = request.getRequestDispatcher("editGPA.jsp?msg="+msg);
				 rd.forward(request, response);
                               
                                 
				}
                                        
				catch(Exception e){
				 e.printStackTrace();
                                 String err="Problem occured try again";
                                 rd = request.getRequestDispatcher("editGPA.jsp?msg="+err);
                                
				}
				}
            
            
           else{
               out.println("No request");
           }
        
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
