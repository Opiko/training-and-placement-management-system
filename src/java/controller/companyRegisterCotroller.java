/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;

import static java.lang.Integer.parseInt;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import libraryy.connection;
import model.*;
/**
 *
 * @author Davey
 */
@WebServlet(name = "companyRegisterCotroller", urlPatterns = {"/companyRegisterCotroller"})
public class companyRegisterCotroller extends HttpServlet {

   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
         //retriving all parameters from register form
                String Cname = request.getParameter("Cname");
                int Phone =parseInt(request.getParameter("Phone")); 
                String CEmail = request.getParameter("CEmail");
		
                String Product = request.getParameter("Product");
                String HRManager = request.getParameter("HRManager");
                String Location = request.getParameter("Location");
		String password = request.getParameter("password");
                String vpassword = request.getParameter("vpassword");    
		String sq = request.getParameter("sq");
		String sa = request.getParameter("sa");
                String Role = request.getParameter("Role");
                
                 
     HttpSession session = request.getSession();
		 if(session!=null)
			 session.setAttribute("Email",CEmail );
                
                //setting all the values in model class
                companyModel cmd= new companyModel();
                
                cmd.setCname(Cname);  
                
                cmd.setCEmail(CEmail);
                cmd.setPhone(Phone);
               
                cmd.setProduct(Product);
                cmd.setHRManager(HRManager);
                cmd.setLocation(Location);
		cmd.setPassword(password);
                cmd.setVpassword(vpassword);
		cmd.setSq(sq);
                cmd.setSa(sa);
		
                cmd.setRole(Role);
                
                myUserModel1 usm1=new myUserModel1();
                 usm1.setFname(Cname);
                  usm1.setLname(HRManager);
                   usm1.setEmail(CEmail);
                    usm1.setPassword(password);
                     usm1.setVPassword(vpassword);
                     usm1.setRole(Role);
                      
                 String sql2="insert into users1 values(?,?,?,?,?,?)";
                 connection.RegisterUser(usm1,sql2);
                 String sqlc="insert into companies values(?,?,?,?,?,?,?,?,?,?,?)";
                 
                int k= connection.registerCompany(cmd,sqlc);
                String message="Welcome Mr. "+HRManager + "We are glad to work with your company";
               if(k!=0){
                   response.sendRedirect("company.jsp?msg="+message);
                   
                   
               }
               else{
                   String message1="Data Not inserted";
                    response.sendRedirect("registerCompany.jsp?msg="+message1);
               }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
