/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

/**
 *
 * @author Davey
 */
public class DeleteAdit extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
             String Email=request.getParameter("Email");
          String action=request.getParameter("action");
          RequestDispatcher rd=null;
          Connection con=null;
            
            if(action.equals("Delete")){  
			 try{
	int option = JOptionPane.showConfirmDialog(null, "Do you want to continue?", "Confirm your action", 
	JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE); 
	     if (option == JOptionPane.YES_OPTION) { 
		  Class.forName("com.mysql.jdbc.Driver");
		   //create connection with the database
		   con = DriverManager.getConnection("jdbc:mysql://localhost:3306/training_placement", "root", "");
             
		   PreparedStatement delete =con.prepareStatement("DELETE FROM mystudents  WHERE Email=? ");
		   delete.setString(1,Email);
		   delete.executeUpdate();
		   int i=delete.executeUpdate();
		 //notify the user that the record has been deleted successfull
		   if(i>0){
	           String success ="Record deleted successfully";
	           JOptionPane.showMessageDialog(null, success);
	           //redirect to back to edit_Staff.jsp page
	           rd = request.getRequestDispatcher("deleteStudent.jsp");
			   rd.forward(request, response);
		       } 
		   //redirect to back to edit_Staff.jsp page
           rd = request.getRequestDispatcher("deleteStudent.jsp");
		   rd.forward(request, response);
		   
		  
	     }
	      else if (option == JOptionPane.NO_OPTION) {
	      //redirect back to the current page if no option is chosen
		  rd = request.getRequestDispatcher("deleteStudent.jsp");
		  rd.forward(request, response); 
	       }
	      else if (option == JOptionPane.CLOSED_OPTION) {  
	      //redirect back to the current page if no option is chosen
		  rd = request.getRequestDispatcher("deleteStudent.jsp");
		  rd.forward(request, response); 
		   con.close();
	      }
		   
		   }
			 
			catch(Exception e){
			 e.printStackTrace();
			} 
		
		}
        
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
