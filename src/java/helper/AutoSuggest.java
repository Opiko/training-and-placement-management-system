package helper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

//import model.RoomModel;

public class AutoSuggest {

	private int totalSites;
	private List<String> Sites;

	public AutoSuggest() throws SQLException {
		ResultSet rs = getValues();
		Sites = new ArrayList<String>();
		while (rs.next()) {
			Sites.add(rs.getString(1));
		}
		totalSites = Sites.size();
	}

	public ResultSet getValues() {
		ResultSet rs = null;
		//RoomModel bm = new RoomModel();
		//rs = bm.getAllSites();
		return rs;
	}

	public List<String> getData(String query) {
		String Site = null;
		query = query.toLowerCase();
		List<String> matched = new ArrayList<String>();
		for (int i = 0; i < totalSites; i++) {
			Site = Sites.get(i).toLowerCase();
			if (Site.startsWith(query)) {
				matched.add(Sites.get(i));
			}
		}
		return matched;
	}

}
