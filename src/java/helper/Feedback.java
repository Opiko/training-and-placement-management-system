package helper;

public class Feedback {
	public String RoomId;
	public String registerSuccess = "You have registered Successfully.";
	public String registerFailed = "Your registration is failed. Please try again..";
	public String loginSuccess = "You have logged in successfully.";
	public String loginFailed = "Invalid Credentials. Please Provide Valid details..";
	public String addNewRoomFailure = "Sorry! Room details addition failed.";
	public String profileUpdateSuccess = "Your profile has been successfully updated";
	public String profileUpdateFailed = "sorry, Your profile has not been updated. Please try again.";
	public String errorOccured = "Sorry. Unknown error occurred";
	public String noResult = "No Result Found!!!";
	public String userContinue = "Do you want to continue? (Y/N)";
	public String userBookingOption = "Do you want to book a receipt now? (Y/N)";
	public String editRoomFailure="Room details edition failed.";
	public String addNewRoomSuccess="Room details added successfully. Your Room ID is ";
	public String editRoomSuccess="Room details edited successfully.";
	public String operatornotavailable="Sorry. Operator not available";
	public String bookSuccess = "Your booking has been completed successfully.";
	public String bookFailure = "sorry. Your booking has been failed. Please try again later.";
	/* Added by basha on 20/10/2014 6.51 PM */
	public String updatePasswordSuccess = "Password updated Successfully.";
	public String updatePasswordFailure = "Sorry. Password updation failed. Please try again.";
	public String accountDeleteSuccess = "Your Account has been Deleted..";
	public String accountDeleteFailure = "Sorry. Your Account is not deleted. Please try again.";
	public String addNewRoomOperatorSuccess = "A new Room is added Successfully..";
	public String addNewRoomoperatorFailed = "Sorry. Adding new Room failed. Please try again.";
	public String passworderror = "Password Error. Please enter correct password.";
	
}
