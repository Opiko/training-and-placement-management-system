package helper;



import controller.LoginController;

public class Validate {

	
	public static boolean phoneNumber(String Phone) {
		int len = Phone.length();
		if (len == 10) {
			return true;
		} else {
			return false;
		}
	}
        public static boolean dob(String dob) {
		int len = dob.length();
		if (len == 4) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean email(String email) {
		String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
		if (email.matches(EMAIL_REGEX)) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean length(String str, int minLen, int maxLen) {
		if (str.length() > minLen && str.length() < maxLen) {
			return true;
		} else {
			return false;
		}
	}

	public static String validateLogin(String UserName, String password) {
		String msg = "";
		
		if (UserName.equalsIgnoreCase("") || UserName == null) {
			msg = msg + "Enter a Valid email Address\n";
			return msg;
		} else if (password.equalsIgnoreCase("") || password == null) {
			msg = msg + "password should not be blank\n";
			
			return msg;
		} else if (!Validate.email(UserName)) {
			msg = msg + "Enter a Valid email Address\n";
			
			return msg;
		} //else if (!Validate.length(UserName, 4, 10)) {
			//msg = msg + "username must be between 4 to 10 characters\n";
			
			//return msg;
		//} 
else if (!Validate.length(password, 4, 20)) {
			msg = msg + "password must be between 4 to 20 character\n";
			
			return msg;
		} else {
			
			return msg;
		}
	}

	public static String validateStudentxxxxProfile(String Fname, String Lname,
			  String Email, String Phone,String dob) {
		String msg = "";
		
		if (Fname.equalsIgnoreCase("") || Fname == null) {
			msg = msg + "First name should not be blank\n";
			
			return msg;
		} else if (Lname.equalsIgnoreCase("") || Lname == null) {
			msg = msg + "Last name should not be blank\n";
			
			return msg;
		}  else if (Email.equalsIgnoreCase("") || Email == null) {
			msg = msg + "email should not be blank\n";
			
			return msg;
		} else if (Phone.equalsIgnoreCase("") || Phone == null) {
			msg = msg + "phone should not be blank\n";
			
			return msg;
		} else if (!Validate.phoneNumber(Phone)) {
			msg = msg + "Phone number must be 10 digist \n";
			
			return msg;
		} else if (!Validate.email(Email)) {
			msg = msg + "username must be valid email\n";
			
			return msg;
		} else if (dob.equalsIgnoreCase("") || dob == null) {
			msg = msg + "Please enter your year of birth\n";
			
			return msg;
		}else if (!Validate.dob(dob)) {
			msg = msg + "Phone number must be 10 digist \n";
			
			return msg;
                }

		return msg;
	}

	/*public static String validateRoomOperatorProfile(String fname,
			String address, String Site, String email, String phone) {
		String msg = "";
		logger.info("Validation for "+email+" : ");
		if (fname.equalsIgnoreCase("") || fname == null) {
			msg = msg + "first name should not be blank\n";
			logger.info(msg);
			return msg;
		} else if (address.equalsIgnoreCase("") || address == null) {
			msg = msg + "address should not be blank\n";
			logger.info(msg);
			return msg;
		} else if (Site.equalsIgnoreCase("") || Site == null) {
			msg = msg + "Site should not be blank\n";
			logger.info(msg);
			logger.info(msg);
			return msg;
		} else if (email.equalsIgnoreCase("") || email == null) {
			msg = msg + "email should not be blank\n";
			logger.info(msg);
			return msg;
		} else if (phone.equalsIgnoreCase("") || phone == null) {
			msg = msg + "phone should not be blank\n";
			logger.info(msg);
			return msg;
		} else if (!Validate.phoneNumber(phone)) {
			msg = msg + "phone number is 10 digit only\n";
			logger.info(msg);
			return msg;
		} else if (!Validate.email(email)) {
			msg = msg + "username must be valid email\n";
			logger.info(msg);
			return msg;
		}
		logger.info(msg);
		return msg;
	}
*/

    public static String validateStudentProfile(String Fname, String Lname, String Email) {
        
        
        String msg = "";
		
		if (Fname.equalsIgnoreCase("") || Fname == null) {
			msg = msg + "First name should not be blank\n";
			
			return msg;
		} else if (Lname.equalsIgnoreCase("") || Lname == null) {
			msg = msg + "Last name should not be blank\n";
			
			return msg;
		}  else if (Email.equalsIgnoreCase("") || Email == null) {
			msg = msg + "email should not be blank\n";
			
			return msg;
		} else if (!Validate.email(Email)) {
			msg = msg + "Enter a Valid email Address\n";
			
			return msg;
		}
		return msg;
    }

}
