
package model;

import java.sql.ResultSet;
import libraryy.connection;

/**
 *
 * @author Davey
 */
public class myUsermodel {
    
    private String Fname;
	private String Lname;
        private String RegNo;
       private String email;
       private String Course;
        private int Phone;
        private int DoB;
        private String password;
        private String vpassword;
       private String Resident;
	private String sq;
	private String sa;
        
        
        public String getFname(){
            return this.Fname;
        }
	public void setFname(String Fname) {
		this.Fname = Fname;
	}
          public String getRegNo(){
            return this.RegNo;
        }

	public void setRegNo(String RegNo) {
		this.RegNo= RegNo;
	}
          public String getLname(){
            return this.Lname;
        }

	public void setLname(String Lname) {
		this.Lname = Lname;
	}
          public String getEmail(){
            return this.email;
        }

	public void setEmail(String email) {
		this.email = email;
	}
          public String getResident(){
            return this.Resident;
        }

	public void setResident(String Resident) {
		this.Resident = Resident;
	}
          public String getCourse (){
            return this.Course;
        }

	public void setCourse(String Course) {
		this.Course = Course;
	}
          public int getPhone(){
            return this.Phone;
        }

	public void setPhone(int Phone) {
		this.Phone = Phone;
	}
         public int getDoB(){
            return this.DoB;
        }

	public void setDoB(int DoB) {
		this.DoB = DoB;
	}
          public String getvpassword(){
            return this.vpassword ;
        }

	public void setvpassword(String vpassword) {
		this.vpassword = vpassword;
	}
          public String getpassword(){
            return this.password;
        }

	public void setPassword(String password) {
		this.password = password;
	}
          public String getSq(){
            return this.sq;
        }
        public void setSq(String sq) {
		this.sq = sq;
	}
          public String getSa(){
            return this.sa;
        }
        public void setSa(String sa) {
		this.sa = sa;
	}
       
        
        
        public ResultSet getAllStudentData()
	{
		connection dbc=new connection();
		String query="select UserName,Fname,Lname,Phone WHERE Role='" + this.Course + "'";
		ResultSet rs=dbc.fireExecuteQuery(query);
		
		return rs;
                
	}

    public boolean deleteuserAccount() {
        connection dbc = new connection();
		String query = "create TRIGGER beforeOperatorDelete"
				+ "BEFORE DELETE ON Roomoperator" + "FOR EACH ROW"
				+ "INSERT INTO operatorhistory" + "select * from user;";
		String query1 = "delete from Roomoperator where userName='" + this.RegNo +"'";
		// System.out.println(query1);
		int numRows = 0;
		numRows = dbc.fireExecuteUpdate(query1);
		dbc.close();
		if (numRows > 0) {
			return true;
		} else {
			return false;
		}
    }
        
}
