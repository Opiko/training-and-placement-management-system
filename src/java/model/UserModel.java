package model;

import java.sql.ResultSet;
import java.sql.SQLException;

import helper.returnClass;

import library.DBConnector;
import library.IDGenerator;

public class UserModel {
	public String Fname;
	public String Lname;
        public String UserName;
        public String Email;
        public String Phone;
        public String Role;
        public String password;
        public String vpassword;
        public String Resident;
	public String sq;
	public String sa;
	

	
        public String getFname(){
            return this.Fname;
        }
	public void setFname(String Fname) {
		this.Fname = Fname;
	}

	public void setUserName(String UserName) {
		this.UserName= UserName;
	}

	public void setLname(String Lname) {
		this.Lname = Lname;
	}

	public void setEmail(String Email) {
		this.Email = Email;
	}

	public void setResident(String Resident) {
		this.Resident = Resident;
	}

	public void setRole(String Role) {
		this.Role = Role;
	}

	public void setPhone(String Phone) {
		this.Phone = Phone;
	}

	public void setvpassword(String vpassword) {
		this.vpassword = vpassword;
	}

	public void setPassword(String password) {
		this.password = password;
	}
        public void setsq(String sq) {
		this.sq = sq;
	}
        public void setsa(String sa) {
		this.sa = sa;
	}

	// insert registration details into user table
	public boolean insertRegistrationData() throws SQLException {
		//this.UserName=generateUserId();
		//String table = "users";
		//String column = "Fname";
		//IDGenerator idg = new IDGenerator();
                DBConnector dbc = new DBConnector();
		//this.Fname = idg.generateId(column, table);
            
		String query = "insert into user values('" + this.Fname + "','"
				+ this.Lname + "','" + this.Email + "','" + this.UserName
				+ "','" + this.Phone + "','"
				+ this.Role + "','" + this.password + "','"
				+ this.vpassword + "','" + this.Resident + "''" + this.sq + "''" + this.sa+ "' ); ";
                int numRows = 0;
                numRows=dbc.fireExecuteUpdate(query);
		
		try {
			
		} catch (Exception e) {
			System.out.println("Email already in use");
			// MainClass.homePage();
		}

		dbc.close();
		if (numRows > 0) {
			return true;
		} else {
			return false;
		}
	}

	// select tuples for matching user-name
	public ResultSet selectLoginData() throws SQLException {
		ResultSet rs = null;
		String query = "select * from users where UserName='" + this.UserName
				+ "' and password='" + this.password + "';";
		DBConnector dbc = new DBConnector();
		
		rs = dbc.fireExecuteQuery(query);
		return rs;
	}

	public boolean selectLoginData1() throws SQLException {
		ResultSet rs = null;
		String query = "select * from user where UserName='" + this.UserName
				+ "' and Password='" + this.password + "';";
		DBConnector dbc = new DBConnector();
		
		rs = dbc.fireExecuteQuery(query);
		if (rs.next()) {
			return true;
		} else {
			return false;
		}
	}
/*
	// update user profile details
	public boolean updateUserProfileData() {
		String query = "update users set Fname = '" + this.Fname
				+ "', Lname = '" + this.Lname + "', Addres = '"
				+ this.Addres + "', Role = '" + this.Role
				+ "', Phone ='" + this.Phone + "', Email ='" + this.Email + "', Resident ='" + this.Resident+ ""
				+ " where userName='" + this.UserName + "';";
		
		DBConnector dbc = new DBConnector();
		int numRows = dbc.fireExecuteUpdate(query);
		if (numRows > 0) {
			dbc.close();
			return true;
		} else {
			dbc.close();
			return false;
		}
	}

	public boolean updatePassword() {
		String query = "update user set password='" + this.password
				+ "' where userID='" + this.userID + "' and password='"
				+ this.oldPassword + "';";
		DBConnector dbc = new DBConnector();
		int numRows = dbc.fireExecuteUpdate(query);
		
		if (numRows >= 1) {
			dbc.close();
			return true;
		} else {
			dbc.close();
			return false;
		}
	}

	public boolean deleteRow() {
		DBConnector dbc = new DBConnector();
		String query = "delete from user where userID='" + this.userID + "';";
		
		int numRows = dbc.fireExecuteUpdate(query);
		if (numRows >= 1) {
			dbc.close();
			return true;
		} else {
			dbc.close();
			return false;
		}
	}

	public boolean updatePasswordByEMail() {
		DBConnector dbc = new DBConnector();
		String query = "update user set password = '" + this.password
				+ "'where email='" + this.email + "'";
		
		int numRows = dbc.fireExecuteUpdate(query);
		if (numRows >= 1) {
			dbc.close();
			return true;
		} else {
			dbc.close();
			return false;
		}
	}

	public boolean deleteUserAccount() {
		DBConnector dbc = new DBConnector();
		
		String query = "delete from user where email='" + this.email + "'";

		
		int numRows = dbc.fireExecuteUpdate(query);
		if (numRows >= 1) {
			dbc.close();
			return true;
		} else {
			dbc.close();
			return false;
		}
	}

	public ResultSet showUserBookings() {
		DBConnector dbc = new DBConnector();
		String query = "select * from booking where userId = '" + this.email
				+ "'";
		System.out.println(query);
		ResultSet rs = dbc.fireExecuteQuery(query);
		return rs;
	}

	public String getUserIDFromEmail(String parameter) throws SQLException {
		// TODO Auto-generated method stub
		DBConnector dbc = new DBConnector();
		ResultSet rs = null;
		String query = "select userid from user where email = \"" + parameter
				+ "\"";
		rs = dbc.fireExecuteQuery(query);
		String userid = "";
		while (rs.next()) {
			userid = rs.getString(1);
		}
		return userid;
	}

	public ResultSet getUserData() {
		ResultSet rs = null;
		DBConnector dbc = new DBConnector();
		String query = "select * from user where userid='" + this.userID + "';";
		System.out.println(query);
		rs = dbc.fireExecuteQuery(query);
		return rs;
	}

	public String getSQ() throws SQLException {
		ResultSet rs = null;
		String str = "";
		String query = "select securityQusetion from user where email='"
				+ this.email + "';";
		DBConnector dbc = new DBConnector();

		rs = dbc.fireExecuteQuery(query);
		System.out.println(query);
		while (rs.next()) {
			str = rs.getString(1);
		}
		rs.close();
		return str;
	}

	public String checkAnswer() throws SQLException {
		String pass="";
		ResultSet rs = null;
		
		String query = "select password from user where email='"+this.email+"' and securityAnswer='"+this.securityAnswer+"'";
		System.out.println(query);
		
		DBConnector dbc = new DBConnector();
		rs= dbc.fireExecuteQuery(query);
		while(rs.next()){
			pass=rs.getString(1);
		}
		rs.close();
		return pass;
	}
*/
}
