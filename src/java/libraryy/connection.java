
package libraryy;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.*;
/**
 *
 * @author Davey
 */
public class connection {

   

   
    private ResultSet rs;
    private Statement stmt;
    private Connection conn;
    private PreparedStatement ps;
    
    
     public static Connection connect() {
         Connection con=null;
         try {
             Class.forName("com.mysql.jdbc.Driver");
             con= (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/training_placement", "root", "");
             
             System.out.println("Connected");
             
         } catch (ClassNotFoundException ex) {
             Logger.getLogger(connection.class.getName()).log(Level.SEVERE, null, ex);
         } 
         catch (SQLException ex) {
             Logger.getLogger(connection.class.getName()).log(Level.SEVERE, null, ex);
         }
       return con;
    }
     
     public static int registerStudent(studentModel stm,String sql){
         int i=0;
         Connection con=connect();
      try{
         PreparedStatement ps=con.prepareStatement(sql);
         ps.setString(1, stm.getFname());    
         ps.setString(2, stm.getLname());
         ps.setString(3, stm.getRegNo());     
         ps.setString(4, stm.getEmail());
         ps.setInt(5, stm.getPhone()); 
         ps.setInt(6, stm.getDoB());
         ps.setString(7, stm.getGender());   
         ps.setString(8, stm.getLevel());
          ps.setString(9, stm.getPassword());
         ps.setString(10, stm.getVPassword());
          ps.setString(11, stm.getSq());
         ps.setString(12, stm.getSa());
        
         
         //ps.setInt(9, stm.getGraduation_Year());
       
        
         i=ps.executeUpdate();
        
      }
      catch(SQLException e){
      e.printStackTrace();
      }
        
         return i;
         
     }
     
      public static int registerCompany(companyModel cmd, String sqlc) {
         int k=0;
         Connection con=connect();
      try{
         PreparedStatement ps=con.prepareStatement(sqlc);
         ps.setString(1, cmd.getCname());
         ps.setInt(2, cmd.getPhone());
         ps.setString(3, cmd.getCEmail());
          
          
         ps.setString(4, cmd.getProduct());
         ps.setString(5, cmd.getHRManager());
         ps.setString(6, cmd.getLocation());
         
         ps.setString(7, cmd.getPassword());
         ps.setString(8, cmd.getVpassword());  
         ps.setString(9, cmd.getSq());
         ps.setString(10, cmd.getSa());
         ps.setString(11, cmd.getRole());
         
         
         k=ps.executeUpdate();
        
      }
      catch(SQLException e){
      e.printStackTrace();
      }
        
         return k;
    }
      
       public static int RegisterUser(myUserModel1 usm1, String sql2) {
         int n=0;
                 Connection con=connect();
           try{
               PreparedStatement ps=con.prepareStatement(sql2);
               ps.setString(1, usm1.getFname());
               ps.setString(2, usm1.getLname());
               ps.setString(3, usm1.getEmail());
               ps.setString(4, usm1.getPassword());
               ps.setString(5, usm1.getVPassword());
                ps.setString(6, usm1.getRole());
               
               n=ps.executeUpdate();
           }
            catch(SQLException e){
      e.printStackTrace();
      }
        return n;
    }
       
 
      
     public ResultSet fireExecuteQuery(String query) {
		rs = null;
               
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return rs;
	}
     

    private PreparedStatement preparedStatement(String sql) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    public int fireExecuteUpdate(String query1) {
        int n = 0;
		try {
			stmt = conn.createStatement();
			n = stmt.executeUpdate(query1);
		} catch (SQLException e) {
			System.out.println(e.getMessage());	
		}
		return n ;
    }

    public void close() {
       try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }

   
}
